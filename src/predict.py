#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/6 9:37

"""
各个模型的预测
"""
import os
import torch
import argparse
import numpy as np
from functools import wraps
from collections import namedtuple
from models.bert import EntityBert
from transformers import BertConfig
from torch.utils.data import DataLoader
from models.model_init import ModelInit
from utils import load_vocab, load_vocab_gaz, predict_write, load_bilstm_model, load_model, convert_tensor_dataset, \
    convert_tensor_dataset_
from models.tiny_bert import TinyBertNamedEntityRecognition
from data_loader import collate_fn, collate_fn_with_gaz, EntityDataset, get_gazetteer, transfer_gaz, to_BIO

# 装饰器
# 功能是使得被修饰后的batch_predict函数返回概率分布preds_list，和对应的序列化的文本信息data_loader


def psx(func):
    @wraps(func)
    # def wrapped_function(*args, psx=False, **kwargs):
    def wrapped_function(args, init_model, data_lst, psx=False):
        if psx:
            global model, gaz
            print(args)
            if args.enhance:
                gaz = get_gazetteer("./datasets/ctb.50d.vec")
            device = args.device
            project_path = os.path.join(args.data_path, args.project_name)
            if args.enhance:
                word2id, tag2id, biword2id, gaz2id, gaz_count = load_vocab_gaz(
                    project_path)
                gaz_count = dict((int(k), v) for k, v in gaz_count.items())
                data_ids, label_ids, masks = transfer_gaz(
                    data_lst, word2id, tag2id, biword2id, gaz2id, gaz, gaz_count)
                dataset = EntityDataset(data_ids, label_ids, masks)
            else:
                word2id, tag2id = load_vocab(project_path)
                dataset = convert_tensor_dataset(args, data_lst, word2id)

            data_loader = DataLoader(dataset=dataset, batch_size=args.batch_size,
                                     collate_fn=collate_fn_with_gaz if args.enhance else collate_fn)
            tokens, preds_list = [], []
            id2tag = {value: to_BIO(key) for key, value in tag2id.items()}
            id2word = {value: key for key, value in word2id.items()}

            for step, batch in enumerate(data_loader):
                if args.model_type == "lstm":
                    model, _ = load_bilstm_model(args, init_model)
                    model_attr = model.module if hasattr(
                        model, 'module') else model
                    if args.enhance:
                        input_ids, _, attention_mask, pred_mask = tuple(
                            [b.to(args.device) for b in batch[i]] if i == 0 else batch[i].to(args.device) for i in
                            range(len(batch)))
                        token = input_ids[0].detach().cpu().numpy()
                    else:
                        input_ids, _, attention_mask, pred_mask = tuple(
                            t.to(args.device) for t in batch)
                        token = input_ids.detach().cpu().numpy()

                    if args.lan:
                        out_size, batch_size = model_attr.out_size, attention_mask.size(
                            0)
                        label_tensor = torch.LongTensor([i for i in range(out_size)]).unsqueeze(0).expand(
                            batch_size, out_size).to(args.device)
                        input_ids = [input_ids, label_tensor]

                    with torch.no_grad():
                        model.eval()
                        logits, preds = model(
                            sents_tensor=input_ids, mask=attention_mask)

                        if args.crf:
                            preds, _ = model_attr.crf.decode(logits)
                        else:
                            preds = preds.detach().cpu().numpy()
                    tokens.extend(token)
                    preds_list.extend(logits)

                elif (args.model_type == "bert" and args.finetune) or (args.model_type == "tinybert"):
                    model = load_model(
                        args, device, len(tag2id)).to(args.device)
                    batch = tuple(t.to(device) for t in batch)
                    input_ids, _, attention_mask, pred_mask = batch
                    with torch.no_grad():
                        model.eval()
                        outputs, _, _, _, preds = model(
                            input_ids=input_ids, attention_mask=attention_mask, pred_mask=pred_mask)
                        # preds = outputs[0].detach().cpu().numpy()
                        preds = preds.detach().cpu().numpy()
                    tokens.extend(input_ids.detach().cpu().numpy())
                    preds_list.extend(outputs)
                    # all_label_mask = pred_mask.detach().cpu().numpy()
                    # all_label_mask = all_label_mask + 0

                elif args.model_type == "bert" and args.distilled:
                    model = load_bilstm_model(args, init_model)
                    batch = tuple(t.to(device) for t in batch)
                    input_ids, _, attention_mask, pred_mask = batch
                    model_attr = model.module if hasattr(
                        model, 'module') else model
                    with torch.no_grad():
                        model_attr.eval()
                        logits, _ = model_attr(
                            sents_tensor=input_ids, mask=attention_mask)
                        preds, _ = model_attr.crf.decode(logits)
                    tokens.extend(input_ids.detach().cpu().numpy())
                    preds_list.extend(logits)
            print("Prediction Done!")
            return preds_list, data_loader
        else:
            return func(args, init_model, data_lst)
    return wrapped_function


@psx
def batch_predict(args, init_model, data_lst):
    global model, gaz
    if args.enhance:
        gaz = get_gazetteer("./datasets/ctb.50d.vec")
    device = args.device
    project_path = os.path.join(args.data_path, args.project_name)
    if args.enhance:
        word2id, tag2id, biword2id, gaz2id, gaz_count = load_vocab_gaz(
            project_path)
        gaz_count = dict((int(k), v) for k, v in gaz_count.items())
        data_ids, label_ids, masks = transfer_gaz(
            data_lst, word2id, tag2id, biword2id, gaz2id, gaz, gaz_count)
        dataset = EntityDataset(data_ids, label_ids, masks)
    else:
        word2id, tag2id = load_vocab(project_path)
        dataset = convert_tensor_dataset(args, data_lst, word2id)

    data_loader = DataLoader(dataset=dataset, batch_size=args.batch_size,
                             collate_fn=collate_fn_with_gaz if args.enhance else collate_fn)

    tokens, preds_list = [], []
    id2tag = {value: to_BIO(key) for key, value in tag2id.items()}
    id2word = {value: key for key, value in word2id.items()}
    for step, batch in enumerate(data_loader):
        if args.model_type == "lstm":
            model, _ = load_bilstm_model(args, init_model)
            model_attr = model.module if hasattr(model, 'module') else model
            if args.enhance:
                input_ids, _, attention_mask, pred_mask = tuple(
                    [b.to(args.device) for b in batch[i]] if i == 0 else batch[i].to(args.device) for i in
                    range(len(batch)))
                token = input_ids[0].detach().cpu().numpy()
            else:
                input_ids, _, attention_mask, pred_mask = tuple(
                    t.to(args.device) for t in batch)
                token = input_ids.detach().cpu().numpy()

            if args.lan:
                out_size, batch_size = model_attr.out_size, attention_mask.size(
                    0)
                label_tensor = torch.LongTensor([i for i in range(out_size)]).unsqueeze(0).expand(
                    batch_size, out_size).to(args.device)
                input_ids = [input_ids, label_tensor]

            with torch.no_grad():
                model.eval()
                logits, preds = model(
                    sents_tensor=input_ids, mask=attention_mask)

                if args.crf:
                    preds, _ = model_attr.crf.decode(logits)
                else:
                    preds = preds.detach().cpu().numpy()
            tokens.extend(token)
            preds_list.extend(preds)

        elif (args.model_type == "bert" and args.finetune) or (args.model_type == "tinybert"):
            model = load_model(args, device, len(tag2id)).to(args.device)
            batch = tuple(t.to(device) for t in batch)
            input_ids, _, attention_mask, pred_mask = batch
            with torch.no_grad():
                model.eval()
                outputs, _, _, _, preds = model(
                    input_ids=input_ids, attention_mask=attention_mask, pred_mask=pred_mask)
                # preds = outputs[0].detach().cpu().numpy()
                preds = preds.detach().cpu().numpy()
            tokens.extend(input_ids.detach().cpu().numpy())
            preds_list.extend(preds)
            # all_label_mask = pred_mask.detach().cpu().numpy()
            # all_label_mask = all_label_mask + 0

        elif args.model_type == "bert" and args.distilled:
            model = load_bilstm_model(args, init_model)
            batch = tuple(t.to(device) for t in batch)
            input_ids, _, attention_mask, pred_mask = batch
            model_attr = model.module if hasattr(model, 'module') else model
            with torch.no_grad():
                model_attr.eval()
                logits, _ = model_attr(
                    sents_tensor=input_ids, mask=attention_mask)
                preds, _ = model_attr.crf.decode(logits)
            tokens.extend(input_ids.detach().cpu().numpy())
            preds_list.extend(preds)
    predict_write(args, tokens, preds_list, id2word, id2tag)

    print("Prediction Done!")


# predict部分划分
def predict_step(model, device, batch, model_type):
    with torch.no_grad():
        model.eval()
        batch = tuple(t.to(device) for t in batch)
        input_ids, _, attention_mask, pred_mask = batch
        model_attr = model.module if hasattr(model, 'module') else model
        if model_type == "lstm_crf" or model_type == "bert_distilled":
            logits, _ = model_attr(sents_tensor=input_ids, mask=attention_mask)
            preds, _ = model_attr.crf.decode(logits)
            preds = np.array(preds)
        elif model_type == "bert_finetune" or model_type == "tinybert":
            _, _, _, _, preds = model(
                input_ids=input_ids, attention_mask=attention_mask, pred_mask=pred_mask)
        elif model_type == "lstm_lan":
            out_size, batch_size = model_attr.out_size, attention_mask.size(0)
            label_tensor = torch.LongTensor([i for i in range(out_size)]).unsqueeze(0).expand(batch_size, out_size).to(
                device)
            input_ids = [input_ids, label_tensor]
            logits, _ = model_attr(sents_tensor=input_ids, mask=attention_mask)
            logits = logits.detach().cpu().numpy()
            preds = np.argmax(logits, axis=2)
        else:
            logits, _ = model_attr(sents_tensor=input_ids, mask=attention_mask)
            logits = logits.detach().cpu().numpy()
            preds = np.argmax(logits, axis=2)
        all_label_mask = pred_mask.detach().cpu().numpy()
        all_label_mask = all_label_mask + 0
        return preds, all_label_mask


def line_predict(model_type: str, data_line, project_name):
    global model, bert_model
    # 构建命名空间
    Namespace = namedtuple("Namespace", ["model_type", "device", "crf", "lan", "enhance", "finetune", "distilled",
                                         "pretrained_model_path", "save_path", "finetune_model_path",
                                         "general_tinybert_model"])
    project_path = os.path.join("./datasets/", project_name)
    device = "cuda" if torch.cuda.is_available() else "cpu"
    word2id, tag2id = load_vocab(project_path)
    id2tag = {value: key for key, value in tag2id.items()}
    dataset = convert_tensor_dataset_(model_type, data_line, word2id)
    preds = None
    all_label_mask = None
    data_loader = DataLoader(
        dataset=dataset, batch_size=32, collate_fn=collate_fn)
    for step, batch in enumerate(data_loader):
        # 判断模型
        model_path = "./save_model"
        if model_type == "lstm":
            ns = Namespace(model_type="lstm", device=device, crf=False, lan=False, enhance=False, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.lstm
            state_dict = torch.load(model_path + "/lstm.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "lstm_crf":
            ns = Namespace(model_type="lstm", device=device, crf=True, lan=False, enhance=False, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.lstm_crf
            state_dict = torch.load(model_path + "/lstm_crf.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "lstm_lan":
            ns = Namespace(model_type="lstm", device=device, crf=False, lan=True, enhance=False, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.lstm_lan
            state_dict = torch.load(model_path + "/lstm_lan.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "bert_distilled":
            ns = Namespace(model_type="bert", device=device, crf=False, lan=False, enhance=False, finetune=False,
                           distilled=True, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.lstm_crf
            state_dict = torch.load(model_path + "/bert_distilled.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "gazlstm":
            ns = Namespace(model_type="lstm", device=device, crf=False, lan=False, enhance=True, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.gazlstm
            state_dict = torch.load(model_path + "/gaz_lstm.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "gazlstm_crf":
            ns = Namespace(model_type="lstm", device=device, crf=True, lan=False, enhance=True, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.gazlstm_crf
            state_dict = torch.load(model_path + "/gaz_lstm_crf.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        elif model_type == "gazlstm_lan":
            ns = Namespace(model_type="lstm", device=device, crf=False, lan=True, enhance=True, finetune=False,
                           distilled=False, pretrained_model_path="./pytorch_bert_model", save_path="./save_model",
                           finetune_model_path="./finetune_bert_model", general_tinybert_model="./general_tiny_bert")
            init_model = ModelInit(ns, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag,
                                   bidirectional=True, pretrained_data=None)
            model = init_model.gazlstm_lan
            state_dict = torch.load(model_path + "/gaz_lstm_lan.pt")
            model.load_state_dict(state_dict)
            preds, all_label_mask = predict_step(
                model=model, device=device, batch=batch, model_type=model_type)

        if model_type == "bert_finetune":
            config = BertConfig.from_pretrained(
                "./save_model/finetune_bert_model")
            bert_model = EntityBert.from_pretrained("./save_model/finetune_bert_model", config=config,
                                                    num_labels=len(tag2id),
                                                    dropout_rate=0.2)
            preds, all_label_mask = predict_step(
                model=bert_model, device=device, batch=batch, model_type=model_type)

        elif model_type == "tinybert":
            config = BertConfig.from_pretrained("./save_model/tiny_bert_model")
            bert_model = TinyBertNamedEntityRecognition.from_pretrained("./save_model/tiny_bert_model", config=config,
                                                                        num_entity_labels=len(tag2id))
            preds, all_label_mask = predict_step(
                model=bert_model, device=device, batch=batch, model_type=model_type)

    label_map = {value: key for key, value in tag2id.items()}
    if isinstance(preds, torch.Tensor):
        pred = []
        for i in preds.numpy().tolist()[0]:
            if i == -1:
                pred.append(0)
            else:
                pred.append(i)
        preds = torch.tensor(pred).unsqueeze(0).cpu().numpy()

    preds_lst = [[] for _ in range(preds.shape[0])]
    for i in range(preds.shape[0]):
        for j in range(preds.shape[1]):
            if all_label_mask[i, j] != word2id["[PAD]"] or word2id["[CLS]"] or word2id["[SEP]"]:
                preds_lst[i].append(label_map[preds[i][j]])

    ner_lst = []
    for words, preds in zip([[w for w in data_line]], preds_lst):
        for word, pred in zip(words, preds):
            if pred == 'O':
                continue
            else:
                ner_lst.append((word, pred))
    dic = {}
    labels = {}
    if ner_lst:
        for i, item in enumerate(ner_lst):
            if item[1].startswith('B'):
                label = ""
                end = i + 1
                while end <= len(ner_lst) - 1 and ner_lst[end][1].startswith('I'):
                    end += 1
                ner_type = item[1].split('-')[1]

                if ner_type not in labels.keys():
                    labels[ner_type] = []

                label += ''.join([item[0] for item in ner_lst[i:end]])
                labels[ner_type].append(label)
    dic["text"] = data_line
    dic["entity"] = labels
    return dic


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_type", type=str, default="lstm", choices=["lstm", "bert", "tinybert"],
                        help="Select the type of model required for training.")
    parser.add_argument("--project_name", type=str,
                        default="", help="The name of project.")
    parser.add_argument("--data_name", type=str,
                        default="predict.txt", help="The data name.")
    parser.add_argument("--crf", action='store_true',
                        help="Whether to enable crf.")
    parser.add_argument("--lan", action='store_true',
                        help="Whether to enable lan.")
    parser.add_argument("--enhance", action='store_true',
                        help="Whether to enable vocabulary enhance.")
    parser.add_argument("--finetune", action='store_true',
                        help="Whether to enable bert fine tune.")
    parser.add_argument("--distilled", action='store_true',
                        help="Whether to enable bert to lstm distilled.")
    parser.add_argument("--device", type=str, default="cpu",
                        choices=["cuda:0", "cpu"], help="Select the device.")
    parser.add_argument("--pretrained_model_path", type=str,
                        default="./save_model/finetune_bert_model")
    parser.add_argument("--finetune_model_path", type=str, default="./finetune_bert_model",
                        help="The path of pretrained model to fine tune.")
    parser.add_argument("--general_tinybert_model", type=str, default="./general_tiny_bert",
                        help="The path of general tiny bert pretrained model.")

    # data path, model path, save path
    parser.add_argument("--data_path", type=str,
                        default="./datasets", help="The path of dataset.")
    parser.add_argument("--batch_size", type=int, default=32,
                        help="The size of predict batch.")
    parser.add_argument("--dropout_rate", type=float,
                        default=0.2, help="The rateof dropout.")
    parser.add_argument("--output_file", type=str,
                        default="./output", help="The path of each model to save.")
    parser.add_argument("--save_path", type=str, default="./save_model",
                        help="The path of each model to save.")

    args = parser.parse_args()
    word2id, tag2id = load_vocab(os.path.join("./datasets", args.project_name))
    id2tag = {value: key for key, value in tag2id.items()}
    init_model = ModelInit(args, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag, bidirectional=True,
                           pretrained_data=None)
    # predict data
    lines = []
    path = os.path.join(args.data_path, args.project_name, args.data_name)
    with open(path, "r", encoding="utf-8") as f:
        sentences = f.readlines()
    data_lst = [sent.strip() for sent in sentences if len(sent.strip()) > 0]
    pred = batch_predict(args, init_model, data_lst)
