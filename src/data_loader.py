#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/5 10:02

import os
import torch
import numpy as np
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pad_sequence
from models.gazetteer import Gazetteer

NULLKEY = "-null-"


def data_split(data_path, data_name, project_name, split_rate):
    print("------------ 数据集切分 ------------")
    origin_path = os.path.join(data_path, data_name)
    write_path = os.path.join(data_path, project_name)
    split_rate = split_rate.split(",")
    with open(origin_path, "r", encoding="utf-8") as file, open(write_path + "/train.txt", "a+",
                                                                                encoding="utf-8") as f1, open(
        write_path + "/dev.txt", "a+", encoding="utf-8") as f2, open(write_path + "/test.txt", "a+",
                                                                       encoding="utf-8") as f3:
        sentences = file.read().split("\n\n")
        train_data = sentences[:int(len(sentences) * float(split_rate[0]))]
        dev_data = sentences[
                   int(len(sentences) * float(split_rate[0])):int(
                       len(sentences) * (float(split_rate[0]) + float(split_rate[1])))]
        test_data = sentences[int(len(sentences) * (float(split_rate[0]) + float(split_rate[1]))):]
        for train in train_data:
            f1.write(train + "\n\n")
        for dev in dev_data:
            f2.write(dev + "\n\n")
        for test in test_data:
            f3.write(test + "\n\n")
        print("train_data: ", len(train_data))
        print("dev_data: ", len(dev_data))
        print("test_data: ", len(test_data))
        print("------------ 数据集切分完成 ------------")

def get_vocab(data_lst, label_lst):
    """
    :param data_lst: 读取到train中的所有数据
    :return: word2id, tag2id
    """
    word2id, tag2id = {}, {}
    sent_lst, lab_lst = [], []
    word_special, tag_special = {"[PAD]": 0, "[UNK]": 1, "[CLS]": 2, "[SEP]": 3}, {"[PAD]": 0}
    for word_sent in data_lst:
        word_lst = word_sent.split()
        for word in word_lst:
            if word not in sent_lst:
                sent_lst.append(word)
    for tag_sent in label_lst:
        tag_lst = tag_sent.split()
        for tag in tag_lst:
            if tag not in lab_lst:
                lab_lst.append(tag)

    word2id.update({w: idx + 4 for idx, w in enumerate(sent_lst)})
    tag2id.update({t: idx + 1 for idx, t in enumerate(lab_lst)})

    word_special.update(word2id)
    tag_special.update(tag2id)
    return word_special, tag_special


def get_gazetteer(gaz_file):
    gaz = Gazetteer(lower=False)
    fins = open(gaz_file, 'r', encoding="utf-8").readlines()
    for fin in fins:
        fin = fin.strip().split()[0]
        if fin:
            gaz.insert(fin, "one_source")
    print("Load gaz file: ", gaz_file, " total size:", gaz.size())
    return gaz


def get_gaz_step(words, gaz, gaz_special, gaz_count, next_index):
    w_length = len(words)
    entitys = []
    for idx in range(w_length):
        matched_entity = gaz.enumerateMatchList(words[idx:])
        entitys += matched_entity
        for entity in matched_entity:
            if entity not in gaz_special:
                gaz_special[entity] = next_index
                next_index += 1
            index = gaz_special.get(entity)
            gaz_count[index] = gaz_count.get(index, 0)

    entitys.sort(key=lambda x: -len(x))
    while entitys:
        longest = entitys[0]
        try:
            longest_index = gaz_special.get(longest)
        except:
            raise Exception("do not have this index")
        gaz_count[longest_index] = gaz_count.get(longest_index, 0) + 1

        gazlen = len(longest)
        for i in range(gazlen):
            for j in range(i + 1, gazlen + 1):
                covering_gaz = longest[i:j]
                if covering_gaz in entitys:
                    entitys.remove(covering_gaz)
    return gaz_special, gaz_count, next_index


def get_vocab_gaz(data_lst, label_lst, gaz_file):
    """
    词汇增强中word、biword、gaz_word、label的id
    """
    # word2id, tag2id, biword2id = {}, {}, {}
    sent_lst, lab_lst, sent_bi_lst = [], [], []
    word_special = {"[PAD]": 0, "[UNK]": 1, "[CLS]": 2, "[SEP]": 3}
    tag_special = {"[PAD]": 0}
    biword_special = {"[PAD]": 0, "[UNK]": 1}
    gaz_special = {"[PAD]": 0, "[UNK]": 1}

    gaz = get_gazetteer(gaz_file)
    next_index = len(gaz_special)
    gaz_count = {0: 1, 1: 1}
    print("all sentences size is ", len(data_lst))

    for word_sent, tag_sent in zip(data_lst, label_lst):
        # start = time.time()
        words = word_sent.split()
        tags = tag_sent.split()
        words_len = len(words)
        if words_len != len(tags):
            raise Exception("words and tags are not equal in length")
        for idx in range(words_len):
            if words[idx] not in sent_lst:
                sent_lst.append(words[idx])
            if tags[idx] not in lab_lst:
                lab_lst.append(tags[idx])
            if idx < words_len - 1:
                biword = words[idx] + words[idx + 1]
            else:
                biword = words[idx] + NULLKEY
            if biword not in sent_bi_lst:
                sent_bi_lst.append(biword)
        gaz_special, gaz_count, next_index = get_gaz_step(words, gaz, gaz_special, gaz_count, next_index)
        # print("one sentence use time is: ", time.time()-start)
    word_special.update({w: idx + 4 for idx, w in enumerate(sent_lst)})
    tag_special.update({t: idx + 1 for idx, t in enumerate(lab_lst)})
    biword_special.update({bw: idx + 2 for idx, bw in enumerate(sent_bi_lst)})
    # word_special.update(word2id)
    # tag_special.update(tag2id)

    return word_special, tag_special, biword_special, gaz_special, gaz, gaz_count


# 数据读取
def _read_entity_file(input_file):
    """
    :param input_file: 输入的数据文件(train, dev, test)
    :return:
    """
    with open(input_file, "r", encoding="utf-8") as f:
        contents = []
        labels = []
        sentences = f.read().split("\n\n")
        for sentence in sentences:
            lines = sentence.split("\n")
            if len(lines) < 2:
                continue
            new_line = []
            new_label = []
            for line in lines:
                if len(line) > 0:
                    new_line.append(line.split("\t")[0].strip())
                    new_label.append(line.split("\t")[1].strip())
            contents.append(" ".join(new_line))
            labels.append(" ".join(new_label))
        return contents, labels


# 数据转为BMESO形式并读取数据
def _read_to_BMESO(input_file):
    print("Convert BIO -> BMESO for file:", input_file)
    with open(input_file, "r", encoding="utf-8") as f:
        sentences = f.read().split("\n\n")
    contents = []
    labels = []
    for sentence in sentences:
        lines = sentence.split("\n")
        if len(lines) < 2:
            continue
        new_line = []
        new_label = []
        sent_len = len(lines)
        for idx in range(sent_len):
            word = lines[idx].split('\t')[0].strip()
            label = lines[idx].split('\t')[1].strip()
            new_line.append(word)
            if label == "O":
                new_label.append(label)
            else:
                label_type = label[2:]
                label_l = label[:2]
                if label_l == "B-":
                    if (idx == sent_len - 1) or ("I-" != lines[idx + 1].split('\t')[1].strip()[:2]):
                        new_label.append("S-" + label_type)
                    else:
                        new_label.append("B-" + label_type)
                elif label_l == "I-":
                    if (idx == sent_len - 1) or ("I-" != lines[idx + 1].split('\t')[1].strip()[:2]):
                        new_label.append("E-" + label_type)
                    else:
                        new_label.append("M-" + label_type)
        contents.append(" ".join(new_line))
        labels.append(" ".join(new_label))
    return contents, labels


def transfer(contents, labels, word2id, tag2id, is_bert=False):
    """
    :param contents: 处理过的数据文本
    :param labels:  处理过的标签文本
    :param word2id:  文本词典
    :param tag2id:  标签词典
    :param is_bert: 是否使用Bert模式处理数据
    :return:
    """
    all_lines = []
    all_tags = []
    pred_mask = []
    for idx, (text, label) in enumerate(zip(contents, labels)):
        words = []
        tags = []
        tokens = []
        label_mask = []
        new_line = text.split()
        new_label = label.split()
        if is_bert:
            # Bert最大长度为512
            if len(new_line) > 510:
                new_line = new_line[:510]
                new_label = new_label[:510]
            new_line.insert(0, "[CLS]")
            new_line.append("[SEP]")
            new_label.insert(0, "[CLS]")
            new_label.append("[SEP]")
            for tag in new_label:
                if tag == "[CLS]" or tag == "[SEP]":
                    label_mask.append(0)
                else:
                    label_mask.append(1)
                tags.append(tag2id[tag] if tag in tag2id else tag2id["O"])
            for word in new_line:
                tokens.append(word2id[word] if word in word2id else word2id["[UNK]"])
        else:
            for tag in new_label:
                tags.append(tag2id[tag])
            for word in new_line:
                words.append(word)
                tokens.append(word2id[word] if word in word2id else word2id["[UNK]"])
        all_lines.append(tokens)
        all_tags.append(tags)
        pred_mask.append(label_mask)
    return all_lines, all_tags, pred_mask


def transfer_gaz_step(idx, words, gaz, gaz_special, gaz_count, gaz_ids, gaz_counts, max_gazlist):
    matched_list = gaz.enumerateMatchList(words[idx:])
    matched_length = [len(a) for a in matched_list]
    matched_id = [gaz_special.get(entity, 1) for entity in matched_list]
    for w in range(len(matched_id)):
        w_len = matched_length[w]
        if w_len == 1:  # sigle
            gaz_ids[idx][3].append(matched_id[w])
            gaz_counts[idx][3].append(1)
        else:
            gaz_ids[idx][0].append(matched_id[w])  # begin
            gaz_counts[idx][0].append(gaz_count[matched_id[w]])
            gaz_ids[idx + w_len - 1][2].append(matched_id[w])  # end
            gaz_counts[idx + w_len - 1][2].append(gaz_count[matched_id[w]])
            for j in range(w_len - 2):
                gaz_ids[idx + j + 1][1].append(matched_id[w])  # middle
                gaz_counts[idx + j + 1][1].append(gaz_count[matched_id[w]])

    for l in range(4):
        if not gaz_ids[idx][l]:
            gaz_ids[idx][l].append(0)
            gaz_counts[idx][l].append(1)

        max_gazlist = max(len(gaz_ids[idx][l]), max_gazlist)

    return gaz_ids, gaz_counts, max_gazlist


def transfer_gaz(data_lst, word_special, tag_special, biword_special, gaz_special, gaz, gaz_count, label_lst=None):
    data_ids = []
    label_ids = []
    masks = []
    if not label_lst:
        label_lst = [None]*len(data_lst)
    for word_sent, tag_sent in zip(data_lst, label_lst):

        word_ids, tag_ids, biword_ids, gazs, gaz_masks = [], [], [], [], []

        words = word_sent.split()
        tags = None
        if tag_sent:
            tags = tag_sent.split()
        words_len = len(words)
        gaz_ids = [[[] for i in range(4)] for _ in range(words_len)]
        gaz_counts = [[[] for i in range(4)] for _ in range(words_len)]
        max_gazlist = 0
        for idx in range(words_len):
            word = words[idx]

            if idx < words_len - 1:
                biword = words[idx] + words[idx + 1]
            else:
                biword = words[idx] + NULLKEY
            # 1 为 '[UNK]'
            word_ids.append(word_special.get(word, 1))
            if tag_sent:
                tag = tags[idx]
                tag_ids.append(tag_special.get(tag))
            biword_ids.append(biword_special.get(biword, 1))

            gaz_ids, gaz_counts, max_gazlist = transfer_gaz_step(idx, words, gaz, gaz_special, gaz_count, gaz_ids,
                                                                 gaz_counts, max_gazlist)

        for idx in range(words_len):
            gaz_mask = []
            for l in range(4):
                l_len = len(gaz_ids[idx][l])
                count_set = set(gaz_counts[idx][l])
                if {0} == count_set:
                    gaz_counts[idx][l] = [1] * l_len
                mask = l_len * [0]
                mask += (max_gazlist - l_len) * [1]
                gaz_ids[idx][l] += (max_gazlist - l_len) * [0]
                gaz_counts[idx][l] += (max_gazlist - l_len) * [0]
                gaz_mask.append(mask)

            gaz_masks.append(gaz_mask)

        data_ids.append([word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks])
        label_ids.append(tag_ids)
        masks.append([])

    return data_ids, label_ids, masks


def norm2one(vec):
    root_sum_square = np.sqrt(np.sum(np.square(vec)))
    return vec / root_sum_square


def load_pretrain_emb(embedding_path):
    embedd_dim = -1
    embedd_dict = dict()
    with open(embedding_path, 'r', encoding="utf-8") as file:
        for line in file:
            line = line.strip()
            if len(line) == 0:
                continue
            tokens = line.split()
            if embedd_dim < 0:
                embedd_dim = len(tokens) - 1
            else:
                assert (embedd_dim + 1 == len(tokens))
            embedd = np.empty([1, embedd_dim])
            embedd[:] = tokens[1:]
            embedd_dict[tokens[0]] = embedd
    return embedd_dict, embedd_dim


def build_pretrain_embedding(embedding_path, word_special, norm=True):

    embedd_dict, embedd_dim = load_pretrain_emb(embedding_path)

    scale = np.sqrt(3.0 / embedd_dim)
    pretrain_emb = np.empty([len(word_special), embedd_dim])
    perfect_match = 0
    case_match = 0
    not_match = 0
    pretrain_emb[0, :] = np.random.uniform(-scale, scale, [1, embedd_dim])
    for word, index in word_special.items():
        if word in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word])
            else:
                pretrain_emb[index, :] = embedd_dict[word]
            perfect_match += 1
        elif word.lower() in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word.lower()])
            else:
                pretrain_emb[index, :] = embedd_dict[word.lower()]
            case_match += 1
        else:
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedd_dim])
            not_match += 1
    pretrain_emb = torch.FloatTensor(pretrain_emb)
    pretrained_size = len(embedd_dict)
    print("Embedding:\n   pretrain word:%s, prefect match:%s, case_match:%s, oov:%s, oov%%:%s" % (
        pretrained_size, perfect_match, case_match, not_match, (not_match + 0.) / len(word_special)))
    return pretrain_emb


class EntityDataset(Dataset):
    """
    定义数据读取迭代器
    """

    def __init__(self, sentences, labels, tag_mask):
        self.sentences, self.labels, self.mask = sentences, labels, tag_mask

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, item):
        return self.sentences[item], self.labels[item], self.mask[item]


def collate_fn(data):
    """
    :param data: data[0]:sentences , data[1]:labels
    :return: 序列化的data、记录实际长度的序列、以及label列表
    """
    dataset = []
    data.sort(key=lambda x: len(x[0]), reverse=True)
    data_length = [len(sequence[0]) for sequence in data]
    attention_mask = []
    for i in data_length:
        attention_mask.append([1] * i)
    lines = [i[0] for i in data]
    tags = [i[1] for i in data]
    pred_mask = [i[2] for i in data]
    data = pad_sequence([torch.from_numpy(np.array(line)) for line in lines], batch_first=True, padding_value=0)
    tag = pad_sequence([torch.from_numpy(np.array(label)) for label in tags], batch_first=True, padding_value=0)
    attention_masks = pad_sequence([torch.from_numpy(np.array(mask)) for mask in attention_mask], batch_first=True,
                                   padding_value=0)
    pred_masks = pad_sequence([torch.from_numpy(np.array(mask)) for mask in pred_mask], batch_first=True,
                              padding_value=0)
    dataset.append(data.type(torch.long))
    dataset.append(tag.type(torch.long))
    dataset.append(attention_masks.type(torch.bool))
    dataset.append(pred_masks.type(torch.bool))
    # batch_data = list(zip(data, tag, attention_masks, pred_masks))
    return dataset


def collate_fn_with_gaz(data):
    """
    param: data
    data[0]:sentences {data[0]: [word_ids, biword_ids, gaz_ids, gazs, gaz_counts, gaz_masks]}
    data[1]:labels
    :return: 序列化的data、记录实际长度的序列、以及label列表
    """
    dataset = []
    data.sort(key=lambda x: len(x[0][0]), reverse=True)
    data_length = [len(d[0][0]) for d in data]
    mask = []
    max_seq_len = data_length[0]
    sent_num = len(data_length)
    for length in data_length:
        mask.append([1] * length)
    instances = [i[0] for i in data]
    word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks = [[d[i] for d in instances] for i in
                                                                  range(len(instances[0]))]
    tags = [d[1] for d in data]
    pred_mask = torch.Tensor([d[2] for d in data]).bool()
    gaz_nums = [len(gaz_id[0][0]) for gaz_id in gaz_ids]
    max_gaz_num = max(gaz_nums)

    word_tensor = pad_sequence([torch.from_numpy(np.array(i)) for i in word_ids], batch_first=True,
                               padding_value=0).long()
    tag_tensor = pad_sequence([torch.from_numpy(np.array(i)) for i in tags], batch_first=True, padding_value=0).long()
    biword_tensor = pad_sequence([torch.from_numpy(np.array(i)) for i in biword_ids], batch_first=True,
                                 padding_value=0).long()
    mask_tensor = pad_sequence([torch.from_numpy(np.array(i)) for i in mask], batch_first=True, padding_value=0).bool()

    layer_gaz_tensor = torch.zeros(sent_num, max_seq_len, 4, max_gaz_num).long()
    gaz_count_tensor = torch.zeros(sent_num, max_seq_len, 4, max_gaz_num).float()
    gaz_mask_tensor = torch.ones(sent_num, max_seq_len, 4, max_gaz_num).bool()
    for idx, (length, gaz_num, gaz_id, gaz_count, gaz_mask) in enumerate(zip(data_length, gaz_nums, gaz_ids,
                                                                             gaz_counts, gaz_masks)):
        layer_gaz_tensor[idx, :length, :, :gaz_num] = torch.LongTensor(gaz_id)
        gaz_count_tensor[idx, :length, :, :gaz_num] = torch.FloatTensor(gaz_count)
        gaz_count_tensor[idx, length:] = 1
        gaz_mask_tensor[idx, :length, :, :gaz_num] = torch.BoolTensor(gaz_mask)

    dataset.extend([[word_tensor, biword_tensor, layer_gaz_tensor, gaz_count_tensor, gaz_mask_tensor], tag_tensor,
                    mask_tensor, pred_mask])

    return dataset


# BMESO --> BIO
def to_BIO(value):
    value_new = None
    if value == "O" or value[:2] == "B-" or value[:2] == "I-":
        value_new = value
    elif value[:2] == "S-":
        value_new = "B-"+value[2:]
    elif value[:2] == "M-" or value[:2] == "E-":
        value_new = "I-"+value[2:]
    return value_new