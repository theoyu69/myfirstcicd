#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: Theo Yu
# @time: 2022/3/2 15:46

"""
模型训练完成后, 发现训练好的模型在验证集上的精确度较低, 且判断原因是标注数据质量低, 则建议使用脏数据工具v3版本, 清洗原始的标注数据
"""
import os
import argparse
import numpy as np
from utils import load_vocab
from models.model_init import ModelInit
from predict import batch_predict, ModelInit
from dirty_data_cleaner_v2.helper import _read_training_data, _create_psx_dataset, _find_noisy_data, _create_error_dataset, _create_clean_data_file


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_type", type=str, default="lstm", choices=["lstm", "bert", "tinybert"],
                        help="Select the type of model required for training.")
    parser.add_argument("--project_name", type=str, default="", help="The name of project.")
    parser.add_argument("--data_name", type=str, default="predict.txt", help="The data name.")
    parser.add_argument("--crf", action='store_true', help="Whether to enable crf.")
    parser.add_argument("--lan", action='store_true', help="Whether to enable lan.")
    parser.add_argument("--enhance", action='store_true', help="Whether to enable vocabulary enhance.")
    parser.add_argument("--finetune", action='store_true', help="Whether to enable bert fine tune.")
    parser.add_argument("--distilled", action='store_true', help="Whether to enable bert to lstm distilled.")
    parser.add_argument("--device", type=str, default="cpu", choices=["cuda:0", "cpu"], help="Select the device.")
    parser.add_argument("--pretrained_model_path", type=str, default="./save_model/finetune_bert_model")
    parser.add_argument("--finetune_model_path", type=str, default="./finetune_bert_model",
                        help="The path of pretrained model to fine tune.")
    parser.add_argument("--general_tinybert_model", type=str, default="./general_tiny_bert",
                        help="The path of general tiny bert pretrained model.")

    # data path, model path, save path
    parser.add_argument("--data_path", type=str, default="./datasets", help="The path of dataset.")
    parser.add_argument("--batch_size", type=int, default=32, help="The size of predict batch.")
    parser.add_argument("--dropout_rate", type=float, default=0.2, help="The rateof dropout.")
    parser.add_argument("--output_file", type=str, default="./output", help="The path of each model to save.")
    parser.add_argument("--save_path", type=str, default="./save_model", help="The path of each model to save.")

    # the parameter of dirty_data_cleaner
    parser.add_argument("--dirty_data_name", type=str, default="train.txt", help="The file name of noisy data.")
    parser.add_argument("--threshold_fraction", type=float, default=0.005, help="Decrease the number of detected nosiy data while narrowing the threshold.")
    parser.add_argument("--clean_data_name", type=str, default="clean_data.txt", help="The file name of clean data.")

    args = parser.parse_args()

    print('请注意,在脏数据工具v3版本中,因为需要调用, 模型训练时基于训练数据train.txt生成的字典,所以只支持清洗训练数据train.txt。')
    print("本次清洗工作设定的阈值是%s" %args.threshold_fraction)

    # 读取模型训练时生成的字典
    word2id, tag2id = load_vocab(os.path.join(args.data_path, args.project_name)) 
    id2tag = {value: key for key, value in tag2id.items()} 

    # 模型初始化
    init_model = ModelInit(args, vocab_size=len(word2id), num_labels=len(tag2id), id2tag=id2tag, bidirectional=True,
                           pretrained_data=None) 
    # 读取训练数据
    data_address = os.path.join(args.data_path, args.project_name, args.dirty_data_name)
    data_with_space, data_regular, label_text, label_sequence = _read_training_data(data_address, tag2id)

    # 计算概率预测矩阵psx
    psx, data_loader = batch_predict(args, init_model, data_with_space, psx=True)

    # 目前NER代码,输出的概率预测矩阵中包含了"[PAD]","[CLS]","[SEP]"干扰标签，需要清洗
    index_to_delete = set([tag2id[e] for e in ["[PAD]","[CLS]","[SEP]"]])
    index_to_keep = list(set(tag2id.values()) - index_to_delete)

    # 生成中间数据集，便于查询psx相关信息
    dataset = _create_psx_dataset(data_regular, label_text, label_sequence, word2id, psx, data_loader, index_to_keep)
    # print(dataset[:3])
    # print(dataset[-3:])

    # 生成cleanlab的重要输入变量s,psx
    if not all(y-x==1 for x, y in zip(index_to_keep, index_to_keep[1:])):
        raise Exception("输入错误！\n标签字典中,标签的value必须是严格+1递增的,并且无效标签必须前置 \n正确的样例如下\n\
            {'[PAD]': 0, '[CLS]': 1, '[SEP]': 2, 'O': 3, 'B-LOC': 4, 'I-LOC': 5, 'B-PER': 6, 'I-PER': 7, 'B-ORG': 8, 'I-ORG': 9}")
    s_dict = {e:e-min(index_to_keep) for e in index_to_keep}
    s= [s_dict[j] for i in dataset['label_sequence'] for j in i]
    s = np.array(s)
    psx = [j for i in dataset['psx'] for j in i ]
    psx = np.array(psx)

    # 计算脏数据的位置/索引
    s_label = [str(j) for i in dataset['label'] for j in i]
    _, ordered_label_errors_entity = _find_noisy_data(s, psx, args.threshold_fraction, s_label)

    # 错误实体可视化
    dataset_tmp = _create_error_dataset(dataset, ordered_label_errors_entity, s_label)
    print(dataset_tmp)

    # 生成净化后的文本
    _create_clean_data_file(dataset, ordered_label_errors_entity, args.data_path, args.project_name, args.clean_data_name)

