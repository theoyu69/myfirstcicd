#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time    : 2022/1/12 10:36
# @Author  : SinGaln

"""
训练时的模型初始化
"""
import os
from models.bert import EntityBert
from transformers import BertConfig
from models.bilstm_crf import BiLSTM, BiLSTM_CRF, GazBiLSTM, GazBiLSTM_CRF
from models.bilstm_lan import BiLSTM_LAN, GazBiLSTM_LAN
from utils import get_device
from models.tiny_bert import TinyBertNamedEntityRecognition
from config import LANConfig, LSTMConfig, TrainingConfig, BertConfigs, GAZConfig


class ModelInit(object):
    def __init__(self, args, vocab_size, num_labels, id2tag, bidirectional, pretrained_data):
        self.lstm_config = LSTMConfig()
        self.lan_config = LANConfig()
        self.bert_config = BertConfigs()
        self.gaz_config = GAZConfig()
        self.train_config = TrainingConfig()
        self.device = args.device

        self.id2tag = id2tag
        # Bilstm 模型初始化
        if args.model_type == "lstm" and not args.crf and not args.lan and not args.enhance:
            self.lstm = BiLSTM(vocab_size=vocab_size, out_size=num_labels, emb_size=self.lstm_config.emb_size,
                               hidden_size=self.lstm_config.hidden_size,
                               dropout=self.lstm_config.dropout, num_layers=self.lstm_config.num_layers,
                               bidirectional=bidirectional)
            self.lstm, self.model_device = get_device(self.lstm, self.device)

        # GazBilstm 模型初始化
        elif args.model_type == "lstm" and not args.crf and not args.lan and args.enhance:
            vocab_size, bi_vocab_size, gaz_vocab_size = vocab_size
            pretrained_word, pretrained_biword, pretrained_gaz = pretrained_data
            self.gazlstm = GazBiLSTM(vocab_size=vocab_size, out_size=num_labels, emb_size=self.lstm_config.emb_size,
                                     hidden_size=self.lstm_config.hidden_size, dropout=self.lstm_config.dropout,
                                     num_layers=self.lstm_config.num_layers, bidirectional=bidirectional,
                                     gaz_vocab_size=gaz_vocab_size, bi_vocab_size=bi_vocab_size,
                                     gaz_emb_size=self.gaz_config.gaz_emb_size,
                                     bi_emb_size=self.gaz_config.bi_emb_size, pretrained_word=pretrained_word,
                                     pretrained_biword=pretrained_biword, pretrained_gaz=pretrained_gaz)
            self.gazlstm, self.model_device = get_device(self.gazlstm, self.device)

        # Bilstm_crf模型初始化
        elif (args.model_type == "lstm" and args.crf and not args.lan and not args.enhance) or (
                args.model_type == "bert" and args.distilled):
            self.lstm_crf = BiLSTM_CRF(vocab_size=vocab_size, out_size=num_labels,
                                       emb_size=self.lstm_config.emb_size,
                                       hidden_size=self.lstm_config.hidden_size,
                                       dropout=self.lstm_config.dropout, num_layers=self.lstm_config.num_layers,
                                       bidirectional=bidirectional)
            self.lstm_crf, self.model_device = get_device(self.lstm_crf, self.device)

        # GazBilstm_crf模型初始化
        elif args.model_type == "lstm" and args.crf and not args.lan and args.enhance:
            vocab_size, bi_vocab_size, gaz_vocab_size = vocab_size
            pretrained_word, pretrained_biword, pretrained_gaz = pretrained_data
            self.gazlstm_crf = GazBiLSTM_CRF(vocab_size=vocab_size, out_size=num_labels,
                                             emb_size=self.lstm_config.emb_size,
                                             hidden_size=self.lstm_config.hidden_size, dropout=self.lstm_config.dropout,
                                             num_layers=self.lstm_config.num_layers, bidirectional=bidirectional,
                                             gaz_vocab_size=gaz_vocab_size, bi_vocab_size=bi_vocab_size,
                                             gaz_emb_size=self.gaz_config.gaz_emb_size,
                                             bi_emb_size=self.gaz_config.bi_emb_size,
                                             pretrained_word=pretrained_word, pretrained_biword=pretrained_biword,
                                             pretrained_gaz=pretrained_gaz)
            self.gazlstm_crf, self.model_device = get_device(self.gazlstm_crf, self.device)

        # Bilstm_lan模型初始化
        elif args.model_type == "lstm" and not args.crf and args.lan and not args.enhance:
            self.lstm_lan = BiLSTM_LAN(vocab_size=vocab_size, out_size=num_labels,
                                       emb_size=self.lan_config.emb_size, hidden_size=self.lan_config.hidden_size,
                                       num_attention_head=self.lan_config.num_attention_head,
                                       num_of_lstm_layer=self.lan_config.num_of_lstm_layer,
                                       dropout=self.lan_config.dropout, bidirectional=bidirectional)
            self.lstm_lan, self.model_device = get_device(self.lstm_lan, self.device)

        # GazBilstm_lan模型初始化
        elif args.model_type == "lstm" and not args.crf and args.lan and args.enhance:
            vocab_size, bi_vocab_size, gaz_vocab_size = vocab_size
            pretrained_word, pretrained_biword, pretrained_gaz = pretrained_data
            self.gazlstm_lan = GazBiLSTM_LAN(vocab_size=vocab_size, out_size=num_labels,
                                             emb_size=self.lan_config.emb_size, hidden_size=self.lan_config.hidden_size,
                                             num_attention_head=self.lan_config.num_attention_head,
                                             num_of_lstm_layer=self.lan_config.num_of_lstm_layer,
                                             dropout=self.lan_config.dropout, bidirectional=bidirectional,
                                             gaz_vocab_size=gaz_vocab_size, bi_vocab_size=bi_vocab_size,
                                             gaz_emb_size=self.gaz_config.gaz_emb_size,
                                             bi_emb_size=self.gaz_config.bi_emb_size,
                                             pretrained_word=pretrained_word, pretrained_biword=pretrained_biword,
                                             pretrained_gaz=pretrained_gaz)
            self.gazlstm_lan, self.model_device = get_device(self.gazlstm_lan, self.device)

        # Bert finetune模型初始化
        if args.model_type == "bert" and args.finetune and not args.distilled:
            self.config = BertConfig.from_pretrained(args.pretrained_model_path)
            self.finetune = EntityBert.from_pretrained(args.pretrained_model_path, config=self.config,
                                                       num_labels=num_labels,
                                                       dropout_rate=self.bert_config.dropout_rate, use_crf=args.crf)
            self.finetune, self.model_device = get_device(self.finetune, self.device)

        # Bert distilled模型初始化
        elif args.model_type == "bert" and not args.finetune and args.distilled:
            self.student = BiLSTM_CRF(vocab_size=vocab_size, out_size=num_labels,
                                      emb_size=self.lstm_config.emb_size,
                                      hidden_size=self.lstm_config.hidden_size,
                                      dropout=self.lstm_config.dropout, num_layers=self.lstm_config.num_layers,
                                      bidirectional=bidirectional)
            self.distill_lstm, self.model_device = get_device(self.student, self.device)
            finetune_path = os.path.join(args.save_path, args.finetune_model_path)
            self.config = BertConfig.from_pretrained(finetune_path)
            self.teacher = EntityBert.from_pretrained(finetune_path, config=self.config,
                                                      num_labels=num_labels,
                                                      dropout_rate=self.bert_config.dropout_rate, use_crf=args.crf)
            self.teacher, self.model_device = get_device(self.teacher, self.device)

        # TinyBert蒸馏初始化
        if args.model_type == "tinybert":
            finetune_path = os.path.join(args.save_path, args.finetune_model_path)
            self.teacher_config = BertConfig.from_pretrained(finetune_path)
            self.teacher_model = EntityBert.from_pretrained(finetune_path, config=self.teacher_config,
                                                            num_labels=num_labels,
                                                            dropout_rate=self.bert_config.dropout_rate,
                                                            use_crf=args.crf)
            self.student_model = TinyBertNamedEntityRecognition.from_pretrained(args.general_tinybert_model,
                                                                                num_entity_labels=num_labels)
            self.teacher_model, self.model_device = get_device(self.teacher_model, self.device)
            self.student_model, self.model_device = get_device(self.student_model, self.device)
