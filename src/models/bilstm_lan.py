#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/5 10:10

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import numpy as np


class BiLSTM_LAN(nn.Module):
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, num_attention_head,
                 num_of_lstm_layer, dropout, bidirectional, pretrained_word=None):
        super(BiLSTM_LAN, self).__init__()

        self.out_size = out_size
        # print("out_size: ", out_size)
        if pretrained_word is not None:
            self.word_embedding = nn.Embedding.from_pretrained(pretrained_word, freeze=False)
            self.emb_size = self.word_embedding.weight.size()[1]
        else:
            self.word_embedding = nn.Embedding.from_pretrained(self.random_embedding(vocab_size, emb_size),
                                                               freeze=False)
            self.emb_size = emb_size

        self.label_embedding = nn.Embedding.from_pretrained(
            self.random_embedding(out_size, hidden_size * 2 if bidirectional else hidden_size), freeze=False)

        self.droplstm = nn.Dropout(dropout)

        self.lstm_first = nn.LSTM(self.emb_size, hidden_size, num_layers=1,
                                  batch_first=True, bidirectional=bidirectional)
        self.lstm_layer = nn.LSTM(hidden_size * 4, hidden_size, num_layers=1,
                                  batch_first=True, bidirectional=bidirectional)
        self.self_attention_first = multihead_attention(hidden_size * 2 if bidirectional else hidden_size,
                                                        num_heads=num_attention_head, dropout_rate=dropout
                                                        )
        self.self_attention_last = multihead_attention(hidden_size * 2 if bidirectional else hidden_size,
                                                       num_heads=1, dropout_rate=0)
        self.lstm_attention_stack = nn.ModuleList([LSTM_attention(hidden_size, bidirectional, num_attention_head,
                                                                  dropout) for _ in range(int(num_of_lstm_layer) - 2)])

    def random_embedding(self, vocab_size, embedding_dim):
        pretrain_emb = np.empty([vocab_size, embedding_dim])
        scale = np.sqrt(3.0 / embedding_dim)
        for index in range(vocab_size):
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedding_dim])
        pretrain_emb = torch.FloatTensor(pretrain_emb)
        return pretrain_emb

    def forward(self, sents_tensor, mask, targets=None):
        # sents_tensor: [B, L]; label_tensor: [B, outsize]
        word_tensor, label_tensor = sents_tensor[0], sents_tensor[1]
        # print(label_tensor.size())
        # batch_size = sents_tensor.size(0)
        # out_size = self.label_embedding.weight.size()[0]
        # label_tensor = torch.LongTensor([i for i in range(out_size)]).unsqueeze(0).expand(batch_size, out_size)
        # print(label_tensor.size())
        label_embs = self.label_embedding(label_tensor)
        lstm_out = self.word_embedding(word_tensor)
        lengths = mask.sum(dim=1)
        # lstm_out = pack_padded_sequence(input=lstm_out, lengths=lengths.cpu(), batch_first=True)
        hidden = None
        lstm_out, hidden = self.lstm_first(lstm_out, hidden)
        # lstm_out, _ = pad_packed_sequence(lstm_out, batch_first=True)
        lstm_out = self.droplstm(lstm_out)
        attention_label = self.self_attention_first(lstm_out, label_embs, label_embs)
        # shape [batch_size, seq_length, embedding_dim]
        lstm_out = torch.cat([lstm_out, attention_label], -1)
        # shape [batch_size, seq_length, embedding_dim + label_embeeding_dim]

        # LAN layer
        for layer in self.lstm_attention_stack:
            lstm_out = layer(lstm_out, label_embs, lengths, hidden)
        """
        Last Layer 
        Attention weight calculate loss
        """
        # lstm_out = pack_padded_sequence(input=lstm_out, lengths=lengths.cpu(), batch_first=True)
        lstm_out, hidden = self.lstm_layer(lstm_out, hidden)
        # lstm_out, _ = pad_packed_sequence(lstm_out, batch_first=True)
        lstm_out = self.droplstm(lstm_out)

        scores = self.self_attention_last(lstm_out, label_embs, label_embs, True)

        if targets is not None:
            targets = targets[mask]
            out_size = scores.size(2)
            logits = scores.masked_select(mask.unsqueeze(2).expand(-1, -1, out_size)).contiguous().view(-1, out_size)

            assert logits.size(0) == targets.size(0)
            loss = F.cross_entropy(logits, targets)
            preds = torch.argmax(scores, dim=-1)
            return (scores, preds), loss
        else:
            preds = torch.argmax(scores, dim=-1)
            return scores, preds


class GazBiLSTM_LAN(nn.Module):
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, num_attention_head, num_of_lstm_layer, dropout,
                 bidirectional, gaz_vocab_size, bi_vocab_size, gaz_emb_size, bi_emb_size,
                 pretrained_word=None, pretrained_biword=None, pretrained_gaz=None):
        super(GazBiLSTM_LAN, self).__init__()

        self.out_size = out_size
        if pretrained_word is not None:
            self.word_embedding = nn.Embedding.from_pretrained(pretrained_word, freeze=False)
            self.emb_size = self.word_embedding.weight.size()[1]
        else:
            self.word_embedding = nn.Embedding.from_pretrained(self.random_embedding(vocab_size, emb_size),
                                                               freeze=False)
            self.emb_size = emb_size
        if pretrained_biword is not None:
            self.biword_embedding = nn.Embedding.from_pretrained(pretrained_biword, freeze=False)
            self.bi_emb_size = self.biword_embedding.weight.size()[1]
        else:
            self.biword_embedding = nn.Embedding.from_pretrained(self.random_embedding(bi_vocab_size, bi_emb_size),
                                                                 freeze=False)
            self.bi_emb_size = bi_emb_size
        if pretrained_gaz is not None:
            self.gaz_embedding = nn.Embedding.from_pretrained(pretrained_gaz, freeze=False)
            self.gaz_emb_size = self.gaz_embedding.weight.size()[1]
        else:
            self.gaz_embedding = nn.Embedding.from_pretrained(self.random_embedding(gaz_vocab_size, gaz_emb_size),
                                                              freeze=False)
            self.gaz_emb_size = gaz_emb_size

        self.label_embedding = nn.Embedding.from_pretrained(
            self.random_embedding(out_size, hidden_size * 2 if bidirectional else hidden_size), freeze=False)

        self.droplstm = nn.Dropout(dropout)

        feature_dim = self.emb_size + self.bi_emb_size + 4 * self.gaz_emb_size

        self.lstm_first = nn.LSTM(feature_dim, hidden_size, num_layers=1,
                                  batch_first=True, bidirectional=bidirectional)
        self.lstm_layer = nn.LSTM(hidden_size * 4, hidden_size, num_layers=1,
                                  batch_first=True, bidirectional=bidirectional)
        self.self_attention_first = multihead_attention(hidden_size * 2 if bidirectional else hidden_size,
                                                        num_heads=num_attention_head, dropout_rate=dropout
                                                        )
        self.self_attention_last = multihead_attention(hidden_size * 2 if bidirectional else hidden_size,
                                                       num_heads=1, dropout_rate=0)
        self.lstm_attention_stack = nn.ModuleList([LSTM_attention(hidden_size, bidirectional, num_attention_head,
                                                                  dropout) for _ in range(int(num_of_lstm_layer) - 2)])

    def get_input_data(self, word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks):
        batch_size = word_ids.size()[0]
        seq_len = word_ids.size()[1]
        word_embs = self.word_embedding(word_ids)
        biword_embs = self.biword_embedding(biword_ids)
        word_embs = torch.cat([word_embs, biword_embs], dim=-1)
        # word_embs = self.drop(word_embs)
        # word_inputs_d = word_embs
        gaz_embeds = self.gaz_embedding(gaz_ids)
        # gaz_embeds = self.drop(gaz_embeds)
        gaz_mask = gaz_masks.unsqueeze(-1).repeat(1, 1, 1, 1, self.gaz_emb_size)
        gaz_embeds = gaz_embeds.data.masked_fill_(gaz_mask.data, 0)

        count_sum = torch.sum(gaz_counts, dim=3, keepdim=True)  # (b,l,4,gn)
        count_sum = torch.sum(count_sum, dim=2, keepdim=True)  # (b,l,1,1)
        weights = gaz_counts.div(count_sum)  # (b,l,4,g)
        weights = weights * 4
        weights = weights.unsqueeze(-1)
        gaz_embeds = weights * gaz_embeds  # (b,l,4,g,e)
        gaz_embeds = torch.sum(gaz_embeds, dim=3)  # (b,l,4,e)
        gaz_embeds_cat = gaz_embeds.view(batch_size, seq_len, -1)  # (b,l,4*ge)
        word_input_cat = torch.cat([word_embs, gaz_embeds_cat], dim=-1)  # (b,l,we+4*ge)
        return word_input_cat

    def random_embedding(self, vocab_size, embedding_dim):
        pretrain_emb = np.empty([vocab_size, embedding_dim])
        scale = np.sqrt(3.0 / embedding_dim)
        for index in range(vocab_size):
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedding_dim])
        pretrain_emb = torch.FloatTensor(pretrain_emb)
        return pretrain_emb

    def forward(self, sents_tensor, mask, targets=None):
        # sents_tensor: [B, L]; label_tensor: [B, outsize]
        sents_tensor, label_tensor = sents_tensor
        word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks = sents_tensor
        emb = self.get_input_data(word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks)
        label_embs = self.label_embedding(label_tensor)
        lengths = mask.sum(dim=1)
        # lstm_out = pack_padded_sequence(input=lstm_out, lengths=lengths.cpu(), batch_first=True)
        hidden = None
        lstm_out, hidden = self.lstm_first(emb, hidden)
        # lstm_out, _ = pad_packed_sequence(lstm_out, batch_first=True)
        lstm_out = self.droplstm(lstm_out)
        attention_label = self.self_attention_first(lstm_out, label_embs, label_embs)
        # shape [batch_size, seq_length, embedding_dim]
        lstm_out = torch.cat([lstm_out, attention_label], -1)
        # shape [batch_size, seq_length, embedding_dim + label_embeeding_dim]

        # LAN layer
        for layer in self.lstm_attention_stack:
            lstm_out = layer(lstm_out, label_embs, lengths, hidden)
        """
        Last Layer 
        Attention weight calculate loss
        """
        # lstm_out = pack_padded_sequence(input=lstm_out, lengths=lengths.cpu(), batch_first=True)
        lstm_out, hidden = self.lstm_layer(lstm_out, hidden)
        # lstm_out, _ = pad_packed_sequence(lstm_out, batch_first=True)
        lstm_out = self.droplstm(lstm_out)

        scores = self.self_attention_last(lstm_out, label_embs, label_embs, True)

        if targets is not None:
            targets = targets[mask]
            out_size = scores.size(2)
            logits = scores.masked_select(mask.unsqueeze(2).expand(-1, -1, out_size)).contiguous().view(-1, out_size)

            assert logits.size(0) == targets.size(0)
            loss = F.cross_entropy(logits, targets)
            preds = torch.argmax(scores, dim=-1)
            return (scores, preds), loss
        else:
            preds = torch.argmax(scores, dim=-1)
            return scores, preds


class LSTM_attention(nn.Module):
    def __init__(self, hidden_size, bidirectional, num_attention_head, dropout):
        """初始化参数：
            hidden_size：第一层lstm对应的隐向量的维数（单向）
        """
        super(LSTM_attention, self).__init__()
        self.lstm = nn.LSTM(hidden_size * 4 if bidirectional else hidden_size * 2, hidden_size, num_layers=1,
                            batch_first=True, bidirectional=bidirectional)
        self.label_attn = multihead_attention(hidden_size * 2 if bidirectional else hidden_size, num_attention_head,
                                              dropout_rate=dropout)
        self.droplstm = nn.Dropout(dropout)

    def forward(self, lstm_out, label_embs, lengths, hidden):
        lstm_out = pack_padded_sequence(input=lstm_out, lengths=lengths.cpu(), batch_first=True)
        lstm_out, hidden = self.lstm(lstm_out, hidden)
        lstm_out, _ = pad_packed_sequence(lstm_out)
        lstm_out = self.droplstm(lstm_out.transpose(1, 0))
        # lstm_out (seq_length * batch_size * hidden)
        label_attention_output = self.label_attn(lstm_out, label_embs, label_embs)
        # label_attention_output (batch_size, seq_len, embed_size)
        lstm_out = torch.cat([lstm_out, label_attention_output], -1)
        return lstm_out


class multihead_attention(nn.Module):
    def __init__(self, num_units, num_heads=1, dropout_rate=0, causality=False):
        '''Applies multihead attention.
        Args:
            num_units: A scalar. Attention size.
            dropout_rate: A floating point number.
            causality: Boolean. If true, units that reference the future are masked.
            num_heads: An int. Number of heads.
        '''
        super(multihead_attention, self).__init__()
        self.num_units = num_units
        self.num_heads = num_heads
        self.dropout_rate = dropout_rate
        self.causality = causality
        self.Q_proj = nn.Sequential(nn.Linear(self.num_units, self.num_units), nn.ReLU())
        self.K_proj = nn.Sequential(nn.Linear(self.num_units, self.num_units), nn.ReLU())
        self.V_proj = nn.Sequential(nn.Linear(self.num_units, self.num_units), nn.ReLU())

        self.output_dropout = nn.Dropout(p=self.dropout_rate)

    def forward(self, queries, keys, values, last_layer=False):
        # keys, values: same shape of [N, T_k, C_k]
        # queries: A 3d Variable with s hape of [N, T_q, C_q]
        # Linear projections
        Q = self.Q_proj(queries)  # (N, T_q, C)
        K = self.K_proj(keys)  # (N, T_q, C)
        V = self.V_proj(values)  # (N, T_q, C)
        # Split and concat

        Q_ = torch.cat(torch.chunk(Q, self.num_heads, dim=2), dim=0)  # (h*N, T_q, C/h)
        K_ = torch.cat(torch.chunk(K, self.num_heads, dim=2), dim=0)  # (h*N, T_q, C/h)
        V_ = torch.cat(torch.chunk(V, self.num_heads, dim=2), dim=0)  # (h*N, T_q, C/h)
        # Multiplication
        outputs = torch.bmm(Q_, K_.permute(0, 2, 1))  # (h*N, T_q, T_k)
        # Scale
        outputs = outputs / (K_.size()[-1] ** 0.5)

        # Activation
        if last_layer == False:
            outputs = F.softmax(outputs, dim=-1)  # (h*N, T_q, T_k)
        # Query Masking
        query_masks = torch.sign(torch.abs(torch.sum(queries, dim=-1)))  # (N, T_q)
        query_masks = query_masks.repeat(self.num_heads, 1)  # (h*N, T_q)
        query_masks = torch.unsqueeze(query_masks, 2).repeat(1, 1, keys.size()[1])  # (h*N, T_q, T_k)
        outputs = outputs * query_masks
        # Dropouts
        outputs = self.output_dropout(outputs)  # (h*N, T_q, T_k)
        if last_layer == True:
            return outputs
        # Weighted sum
        outputs = torch.bmm(outputs, V_)  # (h*N, T_q, C/h)
        # Restore shape
        outputs = torch.cat(torch.chunk(outputs, self.num_heads, dim=0), dim=2)  # (N, T_q, C)
        # Residual connection
        outputs += queries

        return outputs
