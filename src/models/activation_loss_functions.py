#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time    : 2022/1/20 10:17
# @Author  : SinGaln

"""
构建基本的激活函数和损失函数
"""

import math
import torch
import torch.nn as nn
import torch.nn.functional as F


def gelu(x):
    """gelu激活函数
        0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))
        Also see https://arxiv.org/abs/1606.08415
    """
    return x * 0.5 * (1.0 + torch.erf(x / math.sqrt(2.0)))


def swish(x):
    return x * torch.sigmoid(x)


# 均方差损失
mes_loss = nn.MSELoss()


# soft label 交叉熵损失
def soft_cross_entropy(predicts, targets):
    student_likelihood = F.log_softmax(predicts, dim=-1)
    targets_prob = F.softmax(targets, dim=-1)
    return (-targets_prob * student_likelihood).mean()


class BertLayerNorm(nn.Module):
    def __init__(self, hidden_size, eps=1e-12):
        """Construct a layernorm module in the TF style (epsilon inside the square root).
        """
        super(BertLayerNorm, self).__init__()
        self.weight = nn.Parameter(torch.ones(hidden_size))
        self.bias = nn.Parameter(torch.zeros(hidden_size))
        self.variance_epsilon = eps

    def forward(self, x):
        u = x.mean(-1, keepdim=True)
        s = (x - u).pow(2).mean(-1, keepdim=True)
        x = (x - u) / torch.sqrt(s + self.variance_epsilon)
        return self.weight * x + self.bias
