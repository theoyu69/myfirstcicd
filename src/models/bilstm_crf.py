#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/5 10:07
import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
from .crf import CRF


class BiLSTM_CRF(nn.Module):
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                 pretrained=None):
        """初始化参数：
            vocab_size:字典的大小
            emb_size:词向量的维数
            hidden_size：隐向量的维数
            out_size:标注的种类
        """
        super(BiLSTM_CRF, self).__init__()
        self.bilstm = BiLSTM(vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                             pretrained)
        self.crf = CRF(out_size, batch_first=True)

    def forward(self, sents_tensor, mask, targets=None):
        # [B, L, out_size]
        if targets is not None:
            scores, _ = self.bilstm(sents_tensor, mask, targets)
            scores = scores[0]
            preds, _ = self.crf.decode(scores, mask)
            preds = [seq + [-1] * (mask.size(1) - len(seq)) for seq in preds]
            preds = torch.tensor(preds).to(sents_tensor.device)
            loss = -1 * (self.crf(emissions=scores, tags=targets, mask=mask.byte(), reduction='mean'))
            batch_size = scores.size(0)
            loss = loss / batch_size
            return (scores, preds), loss
        else:
            scores, _ = self.bilstm(sents_tensor, mask)
            preds = torch.argmax(scores, dim=-1)
            return scores, preds


class GazBiLSTM_CRF(nn.Module):
    # 模型初始化
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                 gaz_vocab_size, bi_vocab_size, gaz_emb_size, bi_emb_size,
                 pretrained_word, pretrained_biword, pretrained_gaz):
        """初始化参数：
            vocab_size:字典的大小
            emb_size:词向量的维数
            hidden_size：隐向量的维数
            out_size:标注的种类
        """
        super(GazBiLSTM_CRF, self).__init__()
        self.bilstm = GazBiLSTM(vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                                gaz_vocab_size, bi_vocab_size, gaz_emb_size, bi_emb_size, pretrained_word, pretrained_biword,
                                pretrained_gaz)
        self.crf = CRF(out_size, batch_first=True)

    # 定义前馈神经网络
    def forward(self, sents_tensor, mask, targets=None):
        if targets is not None:
            scores, _ = self.bilstm(sents_tensor, mask, targets)
            scores = scores[0]
            preds, _ = self.crf.decode(scores, mask)
            preds = [seq + [-1] * (mask.size(1) - len(seq)) for seq in preds]
            preds = torch.tensor(preds).to(targets.device)
            total_loss = -1 * self.crf(scores, targets, mask)
            batch_size = scores.size(0)
            loss = total_loss / batch_size
            return (scores, preds), loss
        else:
            # 预测
            scores, _ = self.bilstm(sents_tensor, mask)
            preds = torch.argmax(scores, dim=-1)
            return scores, preds


class BiLSTM(nn.Module):
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                 pretrained=None):
        """初始化参数：
            vocab_size:字典的大小
            emb_size:词向量的维数
            hidden_size：隐向量的维数
            out_size:标注的种类
            num_layers: 默认为1
        """
        super(BiLSTM, self).__init__()
        self.out_size = out_size
        self.embedding = nn.Embedding(vocab_size, emb_size)
        if pretrained is not None:
            self.embedding = nn.Embedding.from_pretrained(pretrained, freeze=False)
            emb_size = self.embedding.weight.size()[1]
        self.lstm = nn.LSTM(
            input_size=emb_size,
            hidden_size=hidden_size,
            num_layers=num_layers,
            bidirectional=bidirectional,
            batch_first=True
        )
        self.fc = nn.Linear(2 * hidden_size if bidirectional else hidden_size, out_size)
        self.droplstm = nn.Dropout(dropout)
        self.init_weights()

    def init_weights(self):
        nn.init.uniform_(self.embedding.weight)
        nn.init.xavier_normal_(self.fc.weight)
        for weights in [self.lstm.weight_hh_l0, self.lstm.weight_ih_l0]:
            nn.init.orthogonal_(weights)

    def forward(self, sents_tensor, mask, targets=None):
        # emb: [B, L, emb_size]
        emb = self.embedding(sents_tensor)

        # 优化lstm输出存储为连续的权重，减少显存的占用
        self.lstm.flatten_parameters()
        rnn_out, _ = self.lstm(emb)
        # rnn_out:[B, L, hidden_size*2]
        rnn_out = self.droplstm(rnn_out)
        scores = self.fc(rnn_out)  # [B, L, out_size]

        if targets is not None:
            targets = targets[mask]
            out_size = scores.size(2)
            logits = scores.masked_select(mask.unsqueeze(2).expand(-1, -1, out_size)).contiguous().view(-1, out_size)

            assert logits.size(0) == targets.size(0)
            loss = F.cross_entropy(logits, targets)
            preds = torch.argmax(scores, dim=-1)
            return (scores, preds), loss
        else:
            preds = torch.argmax(scores, dim=-1)
            return scores, preds


class GazBiLSTM(nn.Module):
    def __init__(self, vocab_size, out_size, emb_size, hidden_size, dropout, num_layers, bidirectional,
                 gaz_vocab_size, bi_vocab_size, gaz_emb_size, bi_emb_size,
                 pretrained_word=None, pretrained_biword=None, pretrained_gaz=None):
        """初始化参数：
            vocab_size:字典的大小
            emb_size:词向量的维数
            hidden_size：隐向量的维数
            out_size:标注的种类
            num_layers: 默认为1
        """
        super(GazBiLSTM, self).__init__()
        if pretrained_word is not None:
            self.word_embedding = nn.Embedding.from_pretrained(pretrained_word, freeze=False)
            self.emb_size = self.word_embedding.weight.size()[1]
        else:
            self.word_embedding = nn.Embedding.from_pretrained(self.random_embedding(vocab_size, emb_size),
                                                               freeze=False)
            self.emb_size = emb_size
        if pretrained_biword is not None:
            self.biword_embedding = nn.Embedding.from_pretrained(pretrained_biword, freeze=False)
            self.bi_emb_size = self.biword_embedding.weight.size()[1]
        else:
            self.biword_embedding = nn.Embedding.from_pretrained(self.random_embedding(bi_vocab_size, bi_emb_size),
                                                                 freeze=False)
            self.bi_emb_size = bi_emb_size
        if pretrained_gaz is not None:
            self.gaz_embedding = nn.Embedding.from_pretrained(pretrained_gaz, freeze=False)
            self.gaz_emb_size = self.gaz_embedding.weight.size()[1]
        else:
            self.gaz_embedding = nn.Embedding.from_pretrained(self.random_embedding(gaz_vocab_size, gaz_emb_size),
                                                              freeze=False)
            self.gaz_emb_size = gaz_emb_size

        feature_dim = self.emb_size + self.bi_emb_size + 4 * self.gaz_emb_size

        self.lstm = nn.LSTM(
            input_size=feature_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            bidirectional=bidirectional,
            batch_first=True
        )
        self.fc = nn.Linear(2 * hidden_size if bidirectional else hidden_size, out_size)
        self.droplstm = nn.Dropout(dropout)
        self.init_weights()

    def random_embedding(self, vocab_size, embedding_dim):
        pretrain_emb = np.empty([vocab_size, embedding_dim])
        scale = np.sqrt(3.0 / embedding_dim)
        for index in range(vocab_size):
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedding_dim])
        pretrain_emb = torch.FloatTensor(pretrain_emb)
        return pretrain_emb

    def init_weights(self):
        # nn.init.uniform_(self.embedding.weight)
        nn.init.xavier_normal_(self.fc.weight)
        for weights in [self.lstm.weight_hh_l0, self.lstm.weight_ih_l0]:
            nn.init.orthogonal_(weights)

    def get_input_data(self, word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks):
        batch_size = word_ids.size()[0]
        seq_len = word_ids.size()[1]
        word_embs = self.word_embedding(word_ids)
        biword_embs = self.biword_embedding(biword_ids)
        word_embs = torch.cat([word_embs, biword_embs], dim=-1)
        gaz_embeds = self.gaz_embedding(gaz_ids)
        gaz_mask = gaz_masks.unsqueeze(-1).repeat(1, 1, 1, 1, self.gaz_emb_size)
        gaz_embeds = gaz_embeds.data.masked_fill_(gaz_mask.data, 0)

        count_sum = torch.sum(gaz_counts, dim=3, keepdim=True)  # (b,l,4,gn)
        count_sum = torch.sum(count_sum, dim=2, keepdim=True)  # (b,l,1,1)
        weights = gaz_counts.div(count_sum)  # (b,l,4,g)
        weights = weights * 4
        weights = weights.unsqueeze(-1)
        gaz_embeds = weights * gaz_embeds  # (b,l,4,g,e)
        gaz_embeds = torch.sum(gaz_embeds, dim=3)  # (b,l,4,e)
        gaz_embeds_cat = gaz_embeds.view(batch_size, seq_len, -1)  # (b,l,4*ge)
        word_input_cat = torch.cat([word_embs, gaz_embeds_cat], dim=-1)  # (b,l,we+4*ge)
        return word_input_cat

    def forward(self, sents_tensor, mask, targets=None):
        word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks = sents_tensor
        emb = self.get_input_data(word_ids, biword_ids, gaz_ids, gaz_counts, gaz_masks)
        self.lstm.flatten_parameters()
        rnn_out, _ = self.lstm(emb)
        rnn_out = self.droplstm(rnn_out)
        scores = self.fc(rnn_out)
        if targets is not None:
            targets = targets[mask]
            out_size = scores.size(2)
            logits = scores.masked_select(mask.unsqueeze(2).expand(-1, -1, out_size)).contiguous().view(-1, out_size)

            assert logits.size(0) == targets.size(0)
            loss = F.cross_entropy(logits, targets)
            preds = torch.argmax(scores, dim=-1)
            return (scores, preds), loss
        else:
            preds = torch.argmax(scores, dim=-1)
            return scores, preds