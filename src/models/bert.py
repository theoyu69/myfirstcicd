#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/5 10:13

import copy
import torch
import numpy as np
import torch.nn as nn
from .crf import CRF
from transformers import BertModel, BertPreTrainedModel


class EntityBert(BertPreTrainedModel):

    def __init__(self, config, num_labels, dropout_rate, use_crf=True):
        super(EntityBert, self).__init__(config)
        self.config = config
        self.num_labels = num_labels
        self.bert = BertModel(config)
        self.dropout = nn.Dropout(dropout_rate)
        self.classifier = nn.Linear(config.hidden_size, num_labels)

        self.use_crf = use_crf
        if self.use_crf:
            self.crf = CRF(num_labels)

    def forward(self, input_ids, attention_mask=None, token_type_ids=None, pred_mask=None, input_labels=None):
        '''
        input_ids:  (batch_size, max_seq_length)
        attention_mask:  (batch_size, max_seq_length)
        token_type_ids:  (batch_size, max_seq_length)
        pred_mask: (batch_size, max_seq_length)
        input_labels: (batch_size, )
        return: (batch_size, max_seq_length), loss
        '''
        # (batch_size, max_seq_length, hidden_size)
        outputs = self.bert(input_ids=input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids,
                            output_attentions=True)
        sequence_output, attention_output, pooled_output = outputs.last_hidden_state, outputs.attentions, outputs.pooler_output
        sequence_output = self.dropout(sequence_output)
        # (batch_size, max_seq_length, num_labels)
        emissions = self.classifier(sequence_output)

        if self.use_crf:
            # crf model has a problem with mask.
            # to be fixed.
            crf_pred_mask = copy.deepcopy(pred_mask).byte()
            crf_pred_mask[:, 0] = 1
            crf_pred_mask[:, -1] = 1
            crf_seq, _ = self.crf.decode(emissions, crf_pred_mask)
            crf_seq = [seq[1:-1] + [-1] * (pred_mask.size(1) - len(seq) + 2) for seq in crf_seq]
            pred = torch.tensor(crf_seq).to(input_ids.device)
        else:
            pred = torch.argmax(emissions, dim=-1)

        total_loss = 0.
        if input_labels is not None:
            if self.use_crf:
                entity_loss = self.crf(emissions, input_labels, mask=attention_mask.byte(), reduction='mean')
                entity_loss = -1 * entity_loss  # 负对数似然函数
            else:
                entity_loss_function = nn.CrossEntropyLoss()
                # 只需要计算激活部分的loss
                if attention_mask is not None:
                    active_loss = attention_mask.view(-1) == 1
                    active_logits = emissions.view(-1, self.num_labels)[active_loss]
                    active_labels = input_labels.view(-1)[active_loss]
                    entity_loss = entity_loss_function(active_logits, active_labels)
                else:
                    entity_loss = entity_loss_function(emissions.view(-1, self.num_labels), input_labels.view(-1))
            total_loss += entity_loss
        # preds
        output = (emissions,)
        output = (total_loss,) + output
        return output, sequence_output, attention_output, sequence_output, pred
