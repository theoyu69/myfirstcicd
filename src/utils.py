#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author: SinGaln
# @time: 2022/1/5 16:24
import os
import json
import torch
import random
import logging
import numpy as np
from transformers import BertConfig
from models.bert import EntityBert
from data_loader import EntityDataset
from models.tiny_bert import TinyBertNamedEntityRecognition

logger = logging.getLogger(__name__)


def save_bert_model(args, model, save_path):
    """
    :param args: 传入的参数
    :param model: bert模型
    :param save_path: 保存路径
    :return:
    """
    # Save model checkpoint (Overwrite)
    if args.model_type == "bert" and args.finetune:
        save_path = os.path.join(save_path, "finetune_bert_model")
        model_to_save = model.module if hasattr(model, 'module') else model
        model_to_save.save_pretrained(save_path)
        # Save training arguments together with the trained model
        torch.save(args, os.path.join(save_path, 'training_args.bin'))
        logger.info("Saving model checkpoint to %s", save_path)
    elif args.model_type == "tinybert":
        save_path = os.path.join(save_path, "tiny_bert_model")
        if not os.path.exists(save_path):
            os.makedirs(save_path)
            model_to_save = model.module if hasattr(model, 'module') else model
            # model_to_save.save_pretrained(self.args.model_dir)
            model_file = os.path.join(save_path, "pytorch_model.bin")
            config_file = os.path.join(save_path, "config.json")
            # 保存模型参数
            torch.save(model_to_save.state_dict(), model_file)
            # 保存配置文件
            model_to_save.config.to_json_file(config_file)
            # Save training arguments together with the trained model
            torch.save(args, os.path.join(save_path, 'training_args.bin'))
            logger.info("Saving model checkpoint to %s", save_path)


def save_model(args, model, save_path):
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    decode_name = None
    if args.model_type == "lstm" and not args.crf and not args.lan:
        decode_name = ""
    elif args.model_type == "lstm" and args.crf and not args.lan:
        decode_name = "_crf"
    elif args.model_type == "lstm" and not args.crf and args.lan:
        decode_name = "_lan"
    elif args.model_type == "bert" and args.distilled and not args.finetune:
        decode_name = "_distilled"
    save_model_path = "{}/{}{}{}.pt".format(save_path, "gaz_" if args.enhance else "", args.model_type, decode_name)
    model = model.module if hasattr(model, 'module') else model
    torch.save(model.state_dict(), save_model_path)
    logger.info("Saving model checkpoint to %s", save_path)


def get_device(model, device):
    """
    :param model: 初始化模型
    :param device: 传入的device信息
    :return:
    """
    if device == "cuda:0,1":
        model = model
        # model = torch.nn.parallel.DistributedDataParallel(model)
        return model, "cuda"
    elif device == "cuda:0" or device == "cpu" or device == "cuda:1":
        model = model.to(device)
        return model, device


def write_dicts(project_path, word2id, tag2id, biword2id=None, gaz2id=None, gaz_count=None):
    data_dict = {"word2id": word2id, "tag2id": tag2id, "biword2id": biword2id, "gaz2id": gaz2id, "gaz_count": gaz_count}
    for name, value in data_dict.items():
        if value:
            with open(project_path + "/" + name + ".json", "w", encoding="utf-8") as f:
                token2id = json.dumps(value, ensure_ascii=False, indent=2)
                f.write(token2id)


def load_vocab(project_path):
    """
    :param project_path: 项目文件夹
    :return: 文本词典和标签词典
    """
    with open(project_path + "/word2id.json", "r", encoding="utf-8") as f1, open(project_path + "/tag2id.json", "r",
                                                                                 encoding="utf-8") as f2:
        word2id = json.load(f1)
        tag2id = json.load(f2)
        return word2id, tag2id


def load_vocab_gaz(project_path):
    dict_name = ["word2id", "tag2id", "biword2id", "gaz2id", "gaz_count"]
    dict_data = []
    for name in dict_name:
        with open(project_path + "/" + name + ".json", "r", encoding="utf-8") as f:
            token2id = json.load(f)
        dict_data.append(token2id)
    return dict_data


# 加载各个模型
def load_bilstm_model(args, init_model, model_path="./save_model"):
    """
    args: 预测时的超参
    init_model: 初始化的各个模型
    model_path: 模型保存的路径,默认为./save_model
    """
    if args.model_type == "lstm" and not args.crf and not args.lan:
        if not args.enhance:
            model, device = init_model.lstm, init_model.model_device
        else:
            model, device = init_model.gazlstm, init_model.model_device
        state_dict = torch.load("{}/{}lstm.pt".format(model_path, "gaz_" if args.enhance else ""))
        model.load_state_dict(state_dict)
        return model, device
    elif args.model_type == "lstm" and args.crf and not args.lan:
        if not args.enhance:
            model, device = init_model.lstm_crf, init_model.model_device
        else:
            model, device = init_model.gazlstm_crf, init_model.model_device
        state_dict = torch.load("{}/{}lstm_crf.pt".format(model_path, "gaz_" if args.enhance else ""))
        model.load_state_dict(state_dict)
        return model, device
    elif args.model_type == "lstm" and not args.crf and args.lan:
        if not args.enhance:
            model, device = init_model.lstm_lan, init_model.model_device
        else:
            model, device = init_model.gazlstm_lan, init_model.model_device
        state_dict = torch.load("{}/{}lstm_lan.pt".format(model_path, "gaz_" if args.enhance else ""))
        model.load_state_dict(state_dict)
        return model, device
    elif args.model_type == "bert" and args.distilled:
        model, device = init_model.lstm_crf, init_model.model_device
        state_dict = torch.load(model_path + "/bert_distilled.pt")
        model.load_state_dict(state_dict)
        return model
    else:
        raise Exception("Load model: without the model!")

def set_seed(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)

def get_args(model_dir):
    return torch.load(os.path.join(model_dir, 'training_args.bin'))


def load_model(args, device, num_labels):
    # 检查路径是否存在模型
    global model_dir, model
    if args.model_type == "bert" and args.finetune:
        model_dir = os.path.join("./save_model", "finetune_bert_model")
        args1 = get_args(model_dir)
        if not os.path.exists(model_dir):
            raise Exception("Model doesn't exists! Train first!")
        try:
            config = BertConfig.from_pretrained(model_dir)
            model = EntityBert.from_pretrained(model_dir, config=config,
                                               num_labels=num_labels,
                                               dropout_rate=0.2)
            model.to(device)
            model.eval()
            print("***** Model Loaded *****")
        except:
            raise Exception("Some model files might be missing...")

    elif args.model_type == "tinybert":
        model_dir = os.path.join("./save_model", "tiny_bert_model")
        args1 = get_args(model_dir)
        if not os.path.exists(model_dir):
            raise Exception("Model doesn't exists! Train first!")
        try:
            config = BertConfig.from_pretrained(model_dir)
            model = TinyBertNamedEntityRecognition.from_pretrained(model_dir, config=config,
                                                                   num_labels=num_labels)
            model.to(device)
            model.eval()
            print("***** Model Loaded *****")

        except:
            raise Exception("Some model files might be missing...")
    return model


def convert_tensor_dataset(args, lines, word2id):
    all_input_ids = []
    all_attention_mask = []
    all_label_mask = []

    for words in lines:
        tokens = []
        label_mask = []
        tokens_mask = []
        for word in words:
            tokens.append(word2id[word] if word in word2id else word2id["[UNK]"])
            label_mask.append(1)
            tokens_mask.append(1)
        if args.model_type == "lstm":
            all_input_ids.append(tokens)
            all_label_mask.append(label_mask)
            all_attention_mask.append(tokens_mask)
        if args.model_type == "bert":
            if len(tokens) > 510:
                tokens = tokens[:510]
            tokens.insert(0, word2id["[CLS]"])
            tokens.append(word2id["[SEP]"])
            if len(tokens_mask) > 510:
                tokens_mask = tokens_mask[:510]
            tokens_mask.insert(0, 1)
            tokens_mask.append(1)
            if len(label_mask) > 510:
                label_mask = label_mask[:510]
            label_mask.insert(0, 0)
            label_mask.append(0)

            all_input_ids.append(tokens)
            all_label_mask.append(label_mask)
            all_attention_mask.append(tokens_mask)
    return EntityDataset(all_input_ids, all_attention_mask, all_label_mask)


def convert_tensor_dataset_(model_type, lines, word2id):
    all_input_ids = []
    all_attention_mask = []
    all_label_mask = []
    tokens = []
    label_mask = []
    tokens_mask = []
    for words in lines:
        tokens.append(word2id[words] if words in word2id else word2id["[UNK]"])
        label_mask.append(1)
        tokens_mask.append(1)
    if "lstm" in model_type:
        all_input_ids.append(tokens)
        all_label_mask.append(label_mask)
        all_attention_mask.append(tokens_mask)
    if "bert" in model_type:
        if len(tokens) > 510:
            tokens = tokens[:510]
        tokens.insert(0, word2id["[CLS]"])
        tokens.append(word2id["[SEP]"])
        if len(tokens_mask) > 510:
            tokens_mask = tokens_mask[:510]
        tokens_mask.insert(0, 1)
        tokens_mask.append(1)
        if len(label_mask) > 510:
            label_mask = label_mask[:510]
        label_mask.insert(0, 0)
        label_mask.append(0)

        all_input_ids.append(tokens)
        all_label_mask.append(label_mask)
        all_attention_mask.append(tokens_mask)
    return EntityDataset(all_input_ids, all_attention_mask, all_label_mask)


def predict_write(args, tokens, preds_list, id2word, id2tag):
    global file_name

    if not os.path.exists(args.output_file):
        os.makedirs(args.output_file)

    if args.model_type == "lstm" and not args.crf and not args.lan and not args.enhance:
        file_name = args.model_type + "_predict.txt"
    elif args.model_type == "lstm" and args.crf and not args.lan and not args.enhance:
        file_name = args.model_type + "_crf_predict.txt"
    elif args.model_type == "lstm" and not args.crf and args.lan and not args.enhance:
        file_name = args.model_type + "_lan_predict.txt"
    elif args.model_type == "lstm" and not args.crf and not args.lan and args.enhance:
        file_name = args.model_type + "_enhance_predict.txt"
    elif args.model_type == "lstm" and args.crf and not args.lan and args.enhance:
        file_name = args.model_type + "_crf_enhance_predict.txt"
    elif args.model_type == "lstm" and not args.crf and args.lan and args.enhance:
        file_name = args.model_type + "_lan_enhance_predict.txt"
    elif args.model_type == "bert" and args.finetune and not args.distilled:
        file_name = args.model_type + "_finetune_predict.txt"
    elif args.model_type == "bert" and not args.finetune and args.distilled:
        file_name = args.model_type + "_distilled_predict.txt"
    elif args.model_type == "tinybert":
        file_name = args.model_type + "_predict.txt"

    write_path = os.path.join(args.output_file, file_name)

    with open(write_path, "w", encoding="utf-8") as f:
        for word_ids, label_ids in zip(tokens, preds_list):
            ner_lst = []
            data_line = []
            labels_lst = []
            for word_id, pred_id in zip(word_ids, label_ids):
                if pred_id == -1:
                    pred_id = 0
                word = id2word[word_id]
                label = id2tag[pred_id]
                if word_id != 0:
                    data_line.append(word)
                    labels_lst.append(label)
                if label == "O" or word == "[PAD]":
                    continue
            for w, l in zip(data_line, labels_lst):
                if l is None:
                    l = "O"
                ner_lst.append((w, l))
            dic = {}
            labels = {}
            if ner_lst:
                for i, item in enumerate(ner_lst):
                    if item[1] is not None and item[1].startswith('B'):
                        label = ""
                        end = i + 1
                        while end <= len(ner_lst) - 1 and ner_lst[end][1].startswith('I'):
                            end += 1
                        ner_type = item[1].split('-')[1]

                        if ner_type not in labels.keys():
                            labels[ner_type] = []

                        label += ''.join([item[0] for item in ner_lst[i:end]])
                        labels[ner_type].append(label)
            dic["text"] = "".join(data_line)
            dic["entity"] = labels
            f.write(json.dumps(dic, ensure_ascii=False, indent=2))
