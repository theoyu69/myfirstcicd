#!/usr/bin/python
# -*- encoding: utf-8 -*-
#@File    :   word_segmentation.py
#@Time    :   2022/02/21 09:17:15
#@Author  :   Theo Yu

import os
import copy
import argparse
import numpy as np
from fastNLP import DataSet
from datetime import datetime
from helper import _load_entity_data, _consequent_index_of_entity, _consequent_index_of_O, _combine_consequent_index_of_entity_and_O, _seg_words


# 脚本实现的功能：对BIO格式的实体标注文本的进行分词(限制条件：必须保留所有的标注实体，不被切割)
if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset_path", type=str, default="./datasets", required=True, help="Input the path of the datasets folder.")
    parser.add_argument("--data_file", type=str, default="data.txt", required=True, help="Input the name of the data file.")
    parser.add_argument("--cut_word_file_path", type=str, default="./datasets", required=True, help="Input the path of word segmentation folder.")
    parser.add_argument("--cut_word_file", type=str, default="cut_word.txt", required=True, help="Input the name of word segmentation file.")

    args = parser.parse_args()

    start_time = datetime.now()
    print("------------ 分词任务开始时间 %s ------------" % start_time)

    # 从BIO格式的实体文本中读取数据
    contents, labels = _load_entity_data(os.path.join(args.dataset_path, args.data_file))

    # 生成字数据集(句子之间用None间隔开)
    char = []
    for i in contents:
        for j in i:
            char.append(j)
        char.append(None)

    char_label = []
    for i in labels:
        for j in i:
            char_label.append(j)
        char_label.append(None)

    char_dataset = DataSet({'raw_chars':char, 'raw_labels':char_label})

    # 在字数据集中，找出所有实体的索引
    X = np.array(char_dataset.raw_chars) 
    y = np.array(char_dataset.raw_labels) 
    index_of_entity_in_char_dataset = np.where((y != 'O') & (y != None))
    index_of_entity_in_char_dataset = np.array(index_of_entity_in_char_dataset[0])

    # 在字数据集中，找出所有非实体的索引（句子间隔符None不计）
    index_of_O_in_char_dataset = np.where(y == 'O')
    index_of_O_in_char_dataset = np.array(index_of_O_in_char_dataset[0])

    # 分割实体和非实体的索引（以每个实体为最小单位，以连续的非实体为最小单位）
    consequent_index_of_entity_in_char_dataset = _consequent_index_of_entity(index_of_entity_in_char_dataset, y)
    consequent_index_of_O_in_char_dataset = _consequent_index_of_O(index_of_O_in_char_dataset)

    # 合并实体/非实体的索引
    A_list = copy.deepcopy(consequent_index_of_entity_in_char_dataset)
    B_list = copy.deepcopy(consequent_index_of_O_in_char_dataset)
    index_of_entity_and_O = _combine_consequent_index_of_entity_and_O(A_list,B_list)

    # 生成分词文本
    article = _seg_words(index_of_entity_and_O,X)

    # 保存分词文本
    with open(os.path.join(args.cut_word_file_path, args.cut_word_file),'w') as f:
        f.write(article)
    print("分词文本%s已经在%s路径下生成"%(args.cut_word_file, args.cut_word_file_path))

    end_time = datetime.now()
    print("------------ 分词任务结束时间 %s ------------" % end_time)
    print("------------ 分词任务运行时间 %s ------------" % (end_time - start_time))
