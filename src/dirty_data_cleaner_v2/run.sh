#--------------------------------------------
# 适用场景：NER模型尚未训练, 想看看这一批的打标数据的质量, 则建议使用脏数据工具v2版本, 识别标注数据中是否存在歧义标签
#--------------------------------------------
python word_segmentation.py \
  --dataset_path="./datasets" \
  --data_file="data.txt" \
  --cut_word_file_path="./datasets" \
  --cut_word_file="cut_word.txt"

python train_model.py \
  --input_dir="datasets/cut_word.txt" \
  --outp1="model/word2vec.model" \
  --outp2="model/word2vec.wordvectors"

python clean.py \
  --dataset_path="./datasets" \
  --input_data_file="data.txt" \
  --threshold_fraction=0.005 \
  --word_vector="model/word2vec.wordvectors" \
  --output_data_file="clean_data.txt"