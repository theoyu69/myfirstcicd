#!/usr/bin/python
# -*- encoding: utf-8 -*-
#@File    :   clean.py
#@Time    :   2022/02/21 11:17:54
#@Author  :   Theo Yu

import os
import copy
import argparse
import cleanlab
import numpy as np
from fastNLP import DataSet
from datetime import datetime
from fastNLP import Vocabulary
from gensim.models import KeyedVectors
from cleanlab.pruning import get_noise_indices
from sklearn.linear_model import LogisticRegression
from helper import _consequent_index_of_entity, _load_entity_data, _output_dataset


# 脚本功能：完成数据预处理，调用cleanlab包，清洗脏数据
if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset_path", type=str, default="./datasets", required=True, help="Input the path of the datasets folder.")
    parser.add_argument("--input_data_file", type=str, default="data.txt", required=True, help="Input the name of the origin data file.")
    parser.add_argument("--threshold_fraction", type=float, default=0.005, required=True, help="Adjust the threshold of prediction probability of the dirty data.")
    parser.add_argument("--word_vector", type=str, default="model/word2vec.wordvectors", required=True, help="Input the path of the self-trained wordvector.")
    parser.add_argument("--output_data_file", type=str, default="clean_data.txt", required=True, help="Input the name of the clean data file.")

    args = parser.parse_args()

    print("本次清洗工作设定的阈值是%s" %args.threshold_fraction)

    start_time = datetime.now()
    print("------------ 脏数据清洗任务开始时间 %s ------------" % start_time)

    # 从txt文件中读取BIO格式的脏数据
    contents, labels = _load_entity_data(os.path.join(args.dataset_path, args.input_data_file))

    # 生成字数据集
    char = []
    for i in contents:
        for j in i:
            char.append(j)
        char.append(None)

    char_label = []
    for i in labels:
        for j in i:
            char_label.append(j)
        char_label.append(None)

    char_dataset = DataSet({'raw_chars':char, 'raw_labels':char_label})

    # 在字数据集中，找出所有实体的索引
    X = np.array(char_dataset.raw_chars) 
    y = np.array(char_dataset.raw_labels) 
    index_of_entity_in_char_dataset = np.where((y != 'O') & (y != None))
    index_of_entity_in_char_dataset = np.array(index_of_entity_in_char_dataset[0])

    # 以每个实体为最小颗粒度分割实体的索引
    consequent_index_of_entity_in_char_dataset = _consequent_index_of_entity(index_of_entity_in_char_dataset, y)

    # 生成实体数据集
    entity_list = []
    for i in consequent_index_of_entity_in_char_dataset:
        word = ''
        for j in i:
            word = word + X[j]
        entity_list.append(word)

    label_list = []
    for i in consequent_index_of_entity_in_char_dataset:
        label = y[i[0]].split('-')[1]
        label_list.append(label)

    entity_dataset = DataSet({'raw_words':entity_list,'raw_entity_label':label_list, 'words':entity_list, 'sequence_entity_label':label_list})
    print("全文一共标注了%s个实体" %len(entity_dataset))

    # 实体数据集中的实体，和，字数据集中实体拆解的字，有映射关系
    # 生成字典：{key:value} -> {词在实体数据集中的索引：词拆解的文字在字数据集中的索引}
    index_of_entity_in_entity_dataset = list(range(len(entity_dataset)))
    index_dict = dict(zip(index_of_entity_in_entity_dataset,consequent_index_of_entity_in_char_dataset))

    # 建立实体词典
    vocab =  Vocabulary()
    vocab.from_dataset(entity_dataset, field_name='words') # 从该dataset中的chars列建立词典
    vocab.index_dataset(entity_dataset,field_name='words') # 使用vocabulary将chars列转换为index

    entity_vocab = Vocabulary(padding=None, unknown=None)
    entity_vocab.from_dataset(entity_dataset, field_name='sequence_entity_label')
    entity_vocab.index_dataset(entity_dataset, field_name='sequence_entity_label')

    # 使用自训练的词典，生成词向量
    # 优点：因为是自定义词典，所以不存在OOV问题
    wv = KeyedVectors.load(args.word_vector, mmap='r')

    # 文本序列化
    X = []
    for item in entity_dataset.raw_words:
        X.append(wv[item])
    X = np.array(X) # 安全代码，确保X是numpy.ndarray的数据格式
    y = np.array(entity_dataset.sequence_entity_label) # 安全代码，确保X是numpy.ndarray的数据格式

    # 为了使代码简洁，我们使用cleanlab，使用基于逻辑回归分类器的交叉验证，得到预期概率矩阵
    psx = cleanlab.latent_estimation.estimate_cv_predicted_probabilities(
        X, y, clf=LogisticRegression(max_iter=1000, multi_class='auto', solver='lbfgs'))

    # 计算脏数据的位置/索引
    ordered_label_errors = get_noise_indices(
        y,
        psx,
        frac_noise = args.threshold_fraction,
        sorted_index_method='normalized_margin', # Orders label errors 数据排序越靠前，被打上错误标签的概率越大
    )
    ordered_label_errors.sort()

    # 可视化输出歧义数据集
    ambiguity_dataset = DataSet({'entity':[entity_list[i] for i in ordered_label_errors],'ambiguity_label':[label_list[i] for i in ordered_label_errors]})
    print(ambiguity_dataset)
    print('一共检测出%s个歧义实体'% len(ambiguity_dataset))

    # 将脏数据的索引从实体数据集映射到全体数据集
    ordered_label_errors_reindex = []
    for i in ordered_label_errors:
        ordered_label_errors_reindex.append(index_dict[i])

    # 清洗噪音标签
    clean_labels = copy.deepcopy(list(char_dataset.raw_labels))
    for i in ordered_label_errors_reindex:
        for j in i:
            clean_labels[j] = 'O'
    clean_dataset = DataSet({'raw_chars':char,'raw_target':char_label, 'clean_raw_target':clean_labels})

    # 结果写入文件txt
    _output_dataset(args.dataset_path,clean_dataset,args.output_data_file)

    print("------------ 脏数据清洗任务结束时间 %s ------------" % (datetime.now()))
    print("------------ 脏数据清洗任务总计用时 %s ------------" % (datetime.now()-start_time))