#!/usr/bin/python
# -*- encoding: utf-8 -*-
#@File    :   utils.py
#@Time    :   2022/01/29 10:23:46
#@Author  :   Theo Yu

import os
import jieba
import torch
import numpy as np
from fastNLP import DataSet
from torch.utils.data import DataLoader
from cleanlab.pruning import get_noise_indices
from typing import Tuple,List,Dict,Any,Callable

# v2
def _load_entity_data(input_file: str) -> Tuple[List[List[str]], List[List[str]]]:
    """
    脏数据工具辅助函数
    功能：读取实体数据
    :param input_file: BIO格式的实体数据(每个字符对应一行的标签, 句子用空行分隔), 文件存储格式为txt
        海	O
        钓	O
        比	O
        赛	O
        地	O
        点	O
        在	O
        厦	B-LOC
        门	I-LOC
        与	O
        金	B-LOC
        门	I-LOC
        之	O
        间	O
        的	O
        海	O
        域	O
        。	O

        这	O
        座	O
    :return content文字列表, [['海','钓','比','赛'], ... ,['这', '座']]
            labels标签列表 [['O','O','O','O'], ... ,['O', 'O']]
    """
    with open(input_file, "r", encoding="utf-8") as f:
        contents = []
        labels = []
        # 按句子切割
        sentences = f.read().split("\n\n")
        for sentence in sentences:
            # 按字切割
            lines = sentence.split("\n")
            # 安全代码，跳过continue之后的代码，进入新的循环
            if len(lines) < 2:
                continue
            new_line = []
            new_label = []
            for line in lines:
                # 安全代码，确保本行有字
                if len(line) > 0:
                    new_line.append(line.split("\t")[0].strip())
                    new_label.append(line.split("\t")[1].strip())   
            contents.append(new_line)
            labels.append(new_label)
    return contents, labels


def _consequent_index_of_O(index_of_O: List[int]) -> List[List[int]]:
    """
    脏数据工具辅助函数
    功能：将一个列表中的整数元素, 按照是否连续, 切割成多个子列表
    :param index_of_O: 实体索引, 以list, tuple, np.array可迭代的格式存储
    :return: a: 嵌套的列表, 列表的元素是连续的索引
    """
    a = []
    b = []
    for i in index_of_O:
        if len(b) == 0 :
            b.append(i)
        elif index_of_O[-1] == i and i - b[-1] == 1:
            b.append(i)
            a.append(b)
        elif index_of_O[-1] == i:
            a.append(b)
            b = []
            b.append(i)
            a.append(b)
        else:
            if i - b[-1] == 1:
                b.append(i)
            else:
                a.append(b)
                b = []
                b.append(i)
    return a


def _combine_consequent_index_of_entity_and_O(A_list: List[List[int]],B_list: List[List[int]]) -> List[Dict[str,Tuple[List[str],List[int]]]]:
    """
    脏数据工具辅助函数
    功能: 将函数_consequent_index_of_entity和_consequent_index_of_O和输出结果合并
    
    @Parameter
    :param: A_list: _consequent_index_of_entity的输出, B_list: _consequent_index_of_O的输出
    :return: [{'O': (['海', '钓', '比', '赛', '地', '点', '在'], [0, 1, 2, 3, 4, 5, 6])}, {'entity': (['厦', '门'], [7, 8])}]
    """
    index_of_entity_and_O = []
    while len(A_list) > 0 and len(B_list) > 0:
        a = A_list[0]
        b = B_list[0]
        if a[0] > b[-1]:
            index_of_entity_and_O.append({'O':b})
            B_list.remove(b)
        if a[-1] < b[0]:
            index_of_entity_and_O.append({'entity':a})
            A_list.remove(a)
    else:
        if len(A_list) > 0:
            for a in A_list:
                index_of_entity_and_O.append({'entity':a})
        elif len(B_list) > 0:
            for b in B_list:
                index_of_entity_and_O.append({'O':b})
        else:
            pass
    return index_of_entity_and_O


def _seg_words(index_of_entity_and_O: List[Dict[str,Tuple[List[str],List[int]]]], X: np.array) -> str:
    """
    脏数据工具辅助函数
    功能将函数_combine_consequent_index_of_entity_and_O输出结果合并
    
    @Parameter
    :param: index_of_entity_and_O: 函数_combine_consequent_index_of_entity_and_O输出结果, 
            X: 标签文本, 字数据集char_dataset的raw_chars字段
    :return: article 分词结果字符串
    """
    article = ''
    for i in index_of_entity_and_O:
        key  = list(i.keys())[0]
        # 给O标签的文本进行分词
        if key == 'O':
            corpus = i[key]
            line = ''
            for i in corpus:
                line = line + X[i]
            cut_line  = [cutword for cutword in jieba.cut(line, cut_all=False)]
            text = ''
            for i in cut_line:
                text = text +' '+ i

        # 保留实体文本
        if key == 'entity':
            corpus = i[key]
            text = ''
            for i in corpus:
                text = text + X[i]
            text = ' ' + text
        article = article + text
    return article


def _output_dataset(dataset_path: str, dataset: DataSet, file_name: str) -> None:
    """
    脏数据工具辅助函数
    :param dataset_path: clean_data.txt文件的生成路径
           dataset: 清洗后的字数据集, fastNLP包中dataset类的实例化
           file_name: 结果保存文件的名称, 一般为clean_data.txt
    :return
    """
    with open(os.path.join(dataset_path, file_name),'w') as file:
        content = ''
        for data in zip(dataset.raw_chars, dataset.clean_raw_target):
            chars, target = data
            if chars == None:
                text = '\n'
            else:
                text = chars +'\t'+ target +'\n'     
            content = content + text
        file.write(content)
    print('\n%s文件已经生成在%s路径下\n' %(file_name, dataset_path))


# v3
def _consequent_index_of_entity(index_of_entity_in_char_dataset: List[int], y: List[str]) -> List[List[int]]:
    """
    脏数据工具的辅助函数
    功能: 把实体索引按每个实体为最小单元切割
    :param: index_of_entity_in_char_dataset: 实体索引
           y: 标签
    :return: 嵌套的列表, 每个元素是一个实体的索引 ex. [[7, 8],[10, 11],[177], [178]]
    """
    last_prefix = ''
    last_type = ''
    c = []
    result = []
    for i in index_of_entity_in_char_dataset:
        prefix = y[i].split('-')[0]
        type = y[i].split('-')[1]
        # 第一个标签
        if last_prefix == '':
            c.append(i)
        if last_prefix == 'B' and prefix == 'I' and type == last_type:
            c.append(i)
        if last_prefix == 'B' and prefix == 'B' and type != last_type:
            result.append(c)
            c = []
            c.append(i)
        if last_prefix == 'B' and prefix == 'B' and type == last_type:
            result.append(c)
            c = []
            c.append(i)
        if last_prefix == 'I' and prefix == 'I' and type == last_type:
            c.append(i)
        if last_prefix == 'I' and prefix == 'B':
            result.append(c)
            c = []
            c.append(i)        
        last_prefix = y[i].split('-')[0]
        last_type = y[i].split('-')[1]
    result.append(c)
    return result


def _read_training_data(data_address: str, tag2id: Dict[str,int]) -> Tuple[List[str], List[str], List[List[str]], List[List[int]]]:
    """
    脏数据工具的辅助函数
    功能: 读取训练数据train.txt
    :param: data_address: 训练数据的路径
            tag2id: 标签字典
            ex.
            {"[PAD]": 0, "[CLS]": 1, "[SEP]": 2, "O": 3, "B-LOC": 4, "I-LOC": 5, "B-PER": 6, "I-PER": 7, "B-ORG": 8, "I-ORG": 9}
    :return: data_with_space: 字间带空格的句子, 句子的顺序和train.txt一致
            ex. 
            ['她 表 示 相 信 ， 克 林 顿 的 中 国 之 行 会 得 到 美 国 人 民 的 理 解 和 支 持 。',
            '叶 钊 颖 拿 下 来 了 、 葛 菲 ／ 顾 俊 拿 下 来 了 、 龚 智 超 也 拿 下 来 了 … … 而 前 三 场 比 赛 全 取 三 分 的 战 绩 ，
            意 味 着 中 国 女 队 已 经 率 先 从 印 尼 女 队 手 中 夺 回 了 阔 别 四 年 之 久 的 尤 伯 杯 。']  
            data_regular: 句子, 句子的顺序和train.txt一致
            label_text: 标签
            ex.
            ['[UNK]', '守', '[UNK]', '门', '[UNK]', '员', '[UNK]', '：', '[UNK]', '何', '[UNK]', '—', '[UNK]','。']
            label_sequence: 序列化的标签
    """
    with open(data_address, "r", encoding="utf-8") as file:
        sentences = file.read().split("\n\n")   # 读取训练数据
    data_with_space = []
    data_regular = []
    label_text = []
    label_sequence = []
    for sentence in sentences:
        if len(sentence) > 0:
            tmp1 = ''
            tmp2 = ''
            tmp3 = []
            tmp4 = []
            chars = [e for e in sentence.split('\n')]
            for char in chars:
                target = [e for e in char.split('\t')]
                if len(target) > 1:
                    tmp1 = tmp1 + ' '+ target[0]
                    tmp2 = tmp2 + target[0]
                    tmp3.append(target[1])
                    tmp4.append(tag2id[target[1]])
            data_with_space.append(tmp1)
            data_regular.append(tmp2)
            label_text.append(tmp3)
            label_sequence.append(tmp4)
    return data_with_space, data_regular, label_text, label_sequence


def _create_psx_dataset(data_regular: List[str], label_text: List[List[str]], label_sequence: List[List[int]],\
     word2id: Dict[str,int], psx: List[torch.Tensor], data_loader: DataLoader, index_to_keep: List[int]) -> DataSet:
    """
    脏数据工具的辅助函数
    功能: 生成中间数据集, 便于查询概率矩阵相关的一切信息
    :param: data_regular: 句子, 句子的顺序和train.txt一致
            label_text: 标签
            label_sequence: 序列化的标签
            word2id: 文本字典
            psx: 概率预测矩阵
            data_loader: 迭代器 batch_predict函数返回的类
            index_to_keep: 排除[PAD],[CLS],[SEP], 需要保留的标签的位置索引
    :return: dataset: psx相关信息的数据集
            +-------+-----------------+--------+----------------+----------------+--------------------+
            | index | sentence        | length | psx            | label          | label_sequence     |
            +-------+-----------------+--------+----------------+----------------+--------------------+
            | 0     | 海钓比赛地点... | 18     | [[1.4663214... | ['O', 'O', ... | [3, 3, 3, 3, 3,... |
            | 1     | 日俄两国国内... | 40     | [[2.4783082... | ['B-LOC', '... | [4, 4, 3, 3, 3,... |
            | 2     | 克马尔的女儿... | 46     | [[0.5681508... | ['B-PER', '... | [6, 7, 7, 3, 3,... |
            +-------+-----------------+--------+----------------+----------------+--------------------+
    """
    # 第一步、生成sentence2label_dict,sentence2labelsequence_dict字典
    # 得到文本和标签的对应关系
    sentence2label_dict = dict(zip(data_regular, label_text))
    sentence2labelsequence_dict = dict(zip(data_regular, label_sequence))

    # 得到和psx对应的序列文本
    id2word = {v: k for k, v in word2id.items()}    # 生成反向字典
    text_in_sequence = []
    for batch in data_loader:
        input_ids, _, _, _ = batch
        for sentence in input_ids.numpy():
            a = []
            for char in sentence:
                a.append(id2word[char])
            text_in_sequence.append(a)
    
    # 第二步、生成sentence2psx_dict,sentence2len_dict字典
    # psx数据格式转化
    # psx —> psx_sentence
    # List[[torch.Tensor]] —> List[[numpy.ndarray]]
    psx_sentence = []
    for batch in psx:
        psx_sentence.append(batch.numpy())

    # 在概率预测矩阵和data_loader还原的文本中,删除和[PAD],[CLS],[SEP]相关的一切数据：
    clean_psx = []
    clean_text = []
    # 横行
    for vector, text in zip(psx_sentence, text_in_sequence):
        filter = np.array([ e not in ["[UNK]","[PAD]","[CLS]","[SEP]"] for e in text])
        vector = np.array(vector, dtype=object)
        text = np.array(text, dtype=object)
        # 清洗横行
        tmp1 = vector[filter]
        tmp2 = []
        # 清洗纵列
        for i in tmp1:
            tmp2.append([i[e] for e in index_to_keep])
        clean_psx.append(tmp2)
        clean_text.append(text[filter])

    # 得到文本和预测概率矩阵的对应关系
    # 得到文本和句子长度的对应关系
    clean_text_string = []
    clean_text_length = []
    for i in clean_text:
        sentence = ''
        length = len(i)
        for j in i:
            sentence = sentence + j
        clean_text_string.append(sentence)
        clean_text_length.append(length)

    sentence2psx_dict = dict(zip(clean_text_string, clean_psx))
    sentence2len_dict = dict(zip(clean_text_string, clean_text_length))

    # 安全代码
    # clean_text_string是从dataloader中还原的文本
    # data_regular是从train.txt中读取的文本
    assert set(clean_text_string) == set(data_regular), '脏数据工具输入数据和模型训练使用的输入数据必须一致,! 默认为train.txt'
    print("经验证,脏数据工具输入数据和模型训练使用的输入数据一致, 都是train.txt, 输入正确。")

    # 第三步、生成数据集
    dataset_index = list(range(len(data_regular)))
    dataset_sentence = data_regular
    dataset_psx = [sentence2psx_dict[e] for e in data_regular]
    dataset_label = [sentence2label_dict[e] for e in data_regular]
    dataset_label_sequence = [sentence2labelsequence_dict[e] for e in data_regular]
    dataset_length = [sentence2len_dict[e] for e in data_regular]
    dataset = DataSet({'index':dataset_index,'sentence':dataset_sentence,'length':dataset_length,'psx':dataset_psx, 'label':dataset_label, 'label_sequence':dataset_label_sequence})

    # 测试脚本
    # print(dataset_index[:3])
    # print(dataset_sentence[:3])
    # print(dataset_psx[:3])
    # print(dataset_label[:3])
    # print(dataset_label_sequence[:3])
    # print(dataset_length[:3])
    return dataset


def _find_noisy_data(s: np.array, psx: np.array, threshold_fraction: float, s_label: List[str]) -> Tuple[List[int],List[List[int]]]:
    """
    脏数据工具的辅助函数
    功能: 根据输入的概率预测矩阵和歧义标签, 计算噪音数据的索引
    :param: s: 带有歧义数据的序列化标签
            psx: 概率预测矩阵
            threshold_fraction: 脏数据清洗时规定的自定义的阈值, 数值越小, 工具发现的脏数据数量越少, 精度越高
            s_label: 带有歧义数据的文本标签
    :return: ordered_label_errors: 带有歧义标签的字的索引
             ordered_label_errors_entity: 带有歧义标签的实体的索引
    """
    # 测试代码
    # with open('input.txt','w') as f:
    #     content1 = s[:50]
    #     content2 = type(s)
    #     content3 = psx[0:50]
    #     content4 = type(psx)
    #     content5 = threshold_fraction
    #     content6 = type(threshold_fraction)
    #     content7 = s_label[0:50]
    #     content8 = type(s_label)
    #     f.write(str(content1)+'\n'+str(content2)+'\n'+str(content3)+'\n'+str(content4)+'\n'+str(content5)+'\n'+str(content6)+'\n'+str(content7)+'\n'+str(content8))

    ordered_label_errors = get_noise_indices(
        s,
        psx,
        frac_noise = threshold_fraction,
        sorted_index_method='normalized_margin', # Orders label errors 数据排序越靠前，被打上错误标签的概率越大
    )
    ordered_label_errors.sort()

    # 得到所有实体的索引         
    index_of_entity = np.where(s !=0 )
    index_of_entity = np.array(index_of_entity[0])
    consequent_index_of_entity = _consequent_index_of_entity(index_of_entity, s_label)
    print("训练数据一共有多少个实体%s" %len(consequent_index_of_entity))
    ordered_label_errors_entity = []
    for i in consequent_index_of_entity:
        for j in i:
            if j in ordered_label_errors:
                ordered_label_errors_entity.append(i)
                break
            else:
                pass

    print("检查出%s个歧义实体" %len(ordered_label_errors_entity))
    print("以上实体对应%s个歧义标签" %len(ordered_label_errors))

    # 测试代码
    # with open('output.txt','w') as f:
    #     content1 = ordered_label_errors[:50]
    #     content2 = type(ordered_label_errors)
    #     content3 = ordered_label_errors_entity[0:50]
    #     content4 = type(ordered_label_errors_entity)
    #     f.write(str(content1)+'\n'+str(content2)+'\n'+str(content3)+'\n'+str(content4))    

    return ordered_label_errors, ordered_label_errors_entity


def _create_error_dataset(dataset: DataSet, ordered_label_errors_entity: List[List[int]], s_label: List[str]) -> DataSet:
    """
    脏数据工具的辅助函数
    功能: 生成噪音数据的数据集
    :param: dataset: psx相关信息的数据集
            ordered_label_errors_entity: 带有歧义标签的实体的索引
            s_label: 带有歧义数据的文本标签
    :return: dataset_tmp: 歧义数据数据集
            +-------------------------------------+-------------+
            | entity                          | ambiguity_label |
            +-------------------------------------+-------------+
            | 怀柔县                           | LOC            |
            | 茅以升                           | PER            |
            | 华学明                           | PER            |
            | ...                             | ...             |
            +-------------------------------------+-------------+
    """
    error_entity = []
    error_label = []
    chars = [str(j) for i in dataset['sentence'] for j in list(i)]
    for i in ordered_label_errors_entity:
        entity = ''
        label = ''
        for j in i:
            entity = entity + chars[j]
            type = s_label[j].split('-')[1]
            label = type
        error_entity.append(entity)
        error_label.append(label)  
    dataset_tmp = DataSet({'entity':error_entity ,'ambiguity_label': error_label})
    return dataset_tmp




def _create_clean_data_file(dataset: DataSet, ordered_label_errors_entity: List[List[int]], data_path: str, project_name: str, file_name: str) -> None:
    """
    脏数据工具的辅助函数
    功能: 生成净化后的文本
    :param: dataset: psx相关信息的数据集
            ordered_label_errors_entity: 带有歧义标签的实体的索引
            data_path: 数据路径
            project_name: 项目名称
            file_name: 净化后的文本名称 ex. clean_data.txt
    :return: 
    """
    ordered_label_errors_entity_break = []
    for i in ordered_label_errors_entity:
        for j in i:
            ordered_label_errors_entity_break.append(j)
    result = ''
    index_min = 0
    index_max = 0
    for data in dataset:
        sentence = data['sentence']
        length = int(data['length'])
        label = data['label']
        # 这句话的最大index
        index_max = index_min + length - 1
        # 每一句话中的错误标签的索引
        error_index_in_sentence = [e - index_min for e in ordered_label_errors_entity_break if e >= index_min and e <= index_max]
        content = ''
        index = 0
        for char,label in zip(list(sentence),label):
            if index in error_index_in_sentence:
                value = char + '\t' + 'O'+ '\n'
                content = content + value
            else:
                value = char + '\t'+ label
                content = content + value + '\n'
            index =  index + 1
        result = result + content + '\n'
        # 下一句话的第一个字的索引 = 上一句话的最后一个字的索引 + 1
        index_min = index_max + 1

    with open(os.path.join(data_path, project_name, file_name), 'w') as f:
        f.write(result)









