#!/usr/bin/python
# -*- encoding: utf-8 -*-
#@File    :   train_model.py
#@Time    :   2022/02/21 10:30:32
#@Author  :   Theo Yu

import os
import sys
import logging
import argparse
import multiprocessing
from datetime import datetime
from gensim.models import Word2Vec
from gensim.models.word2vec import PathLineSentences


# 脚本实现的功能：训练自定义的词向量
if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # input_dir为输入语料， outp1为输出模型， outp2为vector格式的模型
    parser.add_argument("--input_dir", type=str, default="datasets/cut_word.txt", required=True, help="Input the path of the word segementation file.")
    parser.add_argument("--outp1", type=str, default="model/word2vec.model", help="Input the path of the word2vec.model.")
    parser.add_argument("--outp2", type=str, default="model/word2vec.wordvectors", required=True, help="Input the path of the wordvector.")

    args = parser.parse_args()

    start_time = datetime.now()
    print("------------ 词向量训练任务开始时间 %s ------------" % start_time)
    
    # 安全代码，便于脚本重复运行
    if os.path.exists(args.outp1):
        os.remove(args.outp1)
    if os.path.exists(args.outp2):
        os.remove(args.outp2)

    # 日志信息输出
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)
    logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s')
    logging.root.setLevel(level=logging.INFO)
    logger.info("running %s" % ' '.join(sys.argv))
    
    # 训练模型
    model = Word2Vec(PathLineSentences(args.input_dir),
                     vector_size=100, window=10, min_count=1,
                     workers=multiprocessing.cpu_count(), epochs=10)
    # embedding size:100; 共现窗口大小:10; 保留所有出现的词; 多线程运行; 迭代10次

    # model.save(args.outp1)	# 存储二进制模型文件
    word_vectors = model.wv
    word_vectors.save(args.outp2)   # 存储vector格式的模型

    end_time = datetime.now()
    print("------------ 词向量训练任务结束时间 %s ------------" % end_time)
    print("------------ 词向量训练任务运行时间 %s ------------" % (end_time - start_time))


