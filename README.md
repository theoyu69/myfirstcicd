[![pipeline status](http://datacode.dztech.com/teletubbies/ner/badges/qianyu_test/pipeline.svg)](http://datacode.dztech.com/teletubbies/ner/-/commits/qianyu_test)
[![coverage report](http://datacode.dztech.com/teletubbies/ner/badges/qianyu_test/coverage.svg)](http://datacode.dztech.com/teletubbies/ner/-/commits/qianyu_test)

# Gitlab上开启CI/CD
## 注册Runner
已经注册了，使用docker跑测试，tag是`test`
## 添加CI/CD脚本
在项目中添加ci/cd脚本：`.gitlab-ci.yml`，在python中，典型的示例如下：
```yml
image: python:latest  # 所使用的镜像 名称


# 下面是做一些缓存
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/


# 开始前的准备环境脚本命令
before_script:
  - python --version  # For debugging
  - pip install virtualenv -i https://pypi.doubanio.com/simple
  - virtualenv venv --python=python3.9  # 这里使用python3.9版本的虚拟环境
  - source venv/bin/activate
  - pip install tox -i https://pypi.doubanio.com/simple
  - pip install -r requirements.txt -i https://pypi.doubanio.com/simple


# 任务节点
test:
  tags:
    - test  # 注意这里要填上对应runner的tag，否不会运行
  script:  # 下面是运行命令
    - tox
  coverage: '/^TOTAL\s+\d+\s+\d+\s+(\d+%)$/'  # 从输出中正则匹配出测试覆盖率


# 任务节点
run:
  tags:
    - test
  script:
    - cd src
    - python dirty_data_cleaner_v3.py --model_type lstm --project_name car --data_path datasets --crf --dirty_data_name train.txt --threshold_fraction 0.005
    - cd dirty_data_cleaner_v2
    - sh run.sh
```
gitlab-ci.yml的配置语法请参考官网文件:
http://datacode.dztech.com/help/ci/yaml/index


## 在Python项目中启用tox进行测试管理
tox核心作用是支持创建隔离的Python 环境，在里面可以安装不同版本的Python 解释器与各种依赖库，以此方便开发者做自动化测试、打包、持续集成等事情。

使用tox需要一个配置文件`tox.ini`，示例如下：
```ini
[tox]  # 全局性的配置项
envlist = test,pep  # 定义了 tox 的操作的环境
skip_missing_interpreters = True
skipsdist = True  # tox默认会使用sdist构建包，对于测试来说没有必要，而且构建还会要求存在README、setup.py等文件，并且保证setup.py的格式符合要求等，所以跳过此步
indexserver =
    default = https://pypi.doubanio.com/simple  # 定义确定无范围依赖项和 sdist 安装从何处安装


[testenv]  # testenv 虚拟环境的配置项
install_command = pip install -i https://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com {opts} {packages}  # 安装需要的依赖包，替换原有的安装命令
deps =
    -rrequirements.txt  # 环境依赖项
setenv =
    PYTHONPATH = {toxinidir}/  # 设置python程序运行的环境变量


[testenv:test]  # 继承 testenv 的配置，同时其自身 test 配置项的优先级更高
commands = pytest --cov --cov-report=term  # 用于覆盖率计算


[testenv:pep] 
deps = flake8
commands = flake8 src  # 用于代码静态检查
```

tox的配置语法请参考官网文件:
https://www.osgeo.cn/tox/config.html

在上述配置文件中，使用了两个测试任务，一个是通过`flake8`进行代码静态检查，另一个是用`pytest-cov`进行单元测试
在使用`pytest-cov`进行测试时，需要一个`.coveragerc`的配置文件，该文件内容如下：
```
[run]
omit = tests/*
source = src

[paths]
source = test
```
paths为下的source为项目名称，run下的omit为测试代码路径，source为被测试的代码路径。

tox读取配置文件后，会在隐藏文件夹.tox下生成两个相互隔离的环境，用于运行test和pep任务
```text
.
├── pep             --虚拟环境文件夹
│   ├── bin
│   └── lib
│   └── log
│   └── tmp
└── test            --虚拟环境文件夹
    ├── bin
    └── lib
    └── log   
    └── tmp  
```

## 配置徽章
在README中配置两种徽章：
### ci/cd通过
markdown代码如下：
```markdown
[![pipeline status](http://datacode.dztech.com/teletubbies/test/badges/main/pipeline.svg)](http://datacode.dztech.com/teletubbies/test/-/commits/main)
```
其中将`test`换成具体项目名，例如`mlops`即可  
并且将`main`，换成具体分支名
### 代码覆盖率
markdown代码如下
```markdown
[![coverage report](http://datacode.dztech.com/teletubbies/test/badges/main/coverage.svg)](http://datacode.dztech.com/teletubbies/test/-/commits/main)
```
其中将`test`换成具体项目名，例如`mlops`即可  
并且将`main`，换成具体分支名
